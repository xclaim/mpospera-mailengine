package id.co.telkomsigma.btpns.mprospera.request;

import org.springframework.web.multipart.MultipartFile;

public class AddPRSParamRequest {

    private String parameterId;
    private String paramName;
    private String paramType;
    private String paramGroup;
    private String paramValueString;
    private MultipartFile paramValueByte;
    private String paramDescription;

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamGroup() {
        return paramGroup;
    }

    public void setParamGroup(String paramGroup) {
        this.paramGroup = paramGroup;
    }

    public String getParamValueString() {
        return paramValueString;
    }

    public void setParamValueString(String paramValueString) {
        this.paramValueString = paramValueString;
    }

    public MultipartFile getParamValueByte() {
        return paramValueByte;
    }

    public void setParamValueByte(MultipartFile paramValueByte) {
        this.paramValueByte = paramValueByte;
    }

    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

}