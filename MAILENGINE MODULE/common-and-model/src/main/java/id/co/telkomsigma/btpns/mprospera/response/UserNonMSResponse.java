package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

/**
 * Created by Dzulfiqar on 2/05/2017.
 */
public class UserNonMSResponse extends BaseResponse {

    List<UserNonMSPojo> userList;

    public List<UserNonMSPojo> getUserList() {
        return userList;
    }

    public void setUserList(List<UserNonMSPojo> userList) {
        this.userList = userList;
    }

}