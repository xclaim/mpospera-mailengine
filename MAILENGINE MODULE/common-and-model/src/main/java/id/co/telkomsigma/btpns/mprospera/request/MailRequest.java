package id.co.telkomsigma.btpns.mprospera.request;

public class MailRequest extends BaseRequest{
	
	private String userName;
	private String emailAddress;
	private String swId;
	private boolean isForwarded;
	private String approvalUsername;
	private String note;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public boolean getIsForwarded() {
		return isForwarded;
	}
	public void setIsForwarded(boolean isForwarded) {
		this.isForwarded = isForwarded;
	}
	public String getSwId() {
		return swId;
	}
	public void setSwId(String swId) {
		this.swId = swId;
	}
	public String getApprovalUsername() {
		return approvalUsername;
	}
	public void setApprovalUsername(String approvalUsername) {
		this.approvalUsername = approvalUsername;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
	
	
	

}
