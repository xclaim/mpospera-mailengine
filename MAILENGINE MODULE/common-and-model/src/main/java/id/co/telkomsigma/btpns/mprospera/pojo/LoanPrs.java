package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "LoanPrs", entities = {
		@EntityResult(entityClass = LoanPrs.class, fields = {
				@FieldResult(name = "id",column = "id"),
				@FieldResult(name = "outstandingAmount",column = "outstanding_amt"),
				@FieldResult(name = "currentMargin",column = "current_margin"),
				@FieldResult(name = "installmentAmount",column = "installment_amt"),
				@FieldResult(name = "overdueDays",column = "overdue_days"),
		}) })


@Entity
public class LoanPrs {

	@Id
	private long id;
	private BigDecimal outstandingAmount;
	private BigDecimal currentMargin;
	private BigDecimal  installmentAmount;
	private Long overdueDays;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BigDecimal getOutstandingAmount() {
		return outstandingAmount;
	}
	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	public BigDecimal getCurrentMargin() {
		return currentMargin;
	}
	public void setCurrentMargin(BigDecimal currentMargin) {
		this.currentMargin = currentMargin;
	}
	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public Long getOverdueDays() {
		return overdueDays;
	}
	public void setOverdueDays(Long overdueDays) {
		this.overdueDays = overdueDays;
	}

	
	
	
	
}
