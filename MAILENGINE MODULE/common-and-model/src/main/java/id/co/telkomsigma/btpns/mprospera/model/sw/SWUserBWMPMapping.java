package id.co.telkomsigma.btpns.mprospera.model.sw;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Dzulfiqar on 13/04/2017.
 */
@Entity
@Table(name = "T_SW_USER_BWMP_MAP")
public class SWUserBWMPMapping extends GenericModel {

    private Long id;
    private Long swId;
    private Long userId;
    private Date createdDate;
    private String createdBy;
    private String status;
    private String level;
    private String date;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "level")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}