package id.co.telkomsigma.btpns.mprospera.pojo;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "UserEmail", entities = {
		@EntityResult(entityClass = UserEmail.class, fields = {
				@FieldResult(name = "id",column = "id"),
				@FieldResult(name = "userLogin",column = "user_login"),
				@FieldResult(name = "email",column = "email"),
		}) })
@Entity
public class UserEmail {
	
	@Id
	private Long id;
	private String userLogin;
	private String email;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
