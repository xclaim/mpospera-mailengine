package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

/**
 * Created by Dzulfiqar on 27/04/2017.
 */
public class BusinessTypeAp3rListResponse extends BaseResponse {

    private List<BusinessTypeAp3rList> businessTypeList;
    private String grandTotal;
    private String currentTotal;
    private String totalPage;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<BusinessTypeAp3rList> getBusinessTypeList() {
        return businessTypeList;
    }

    public void setBusinessTypeList(List<BusinessTypeAp3rList> businessTypeList) {
        this.businessTypeList = businessTypeList;
    }

}