package id.co.telkomsigma.btpns.mprospera.pojo;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "PlafonMapping", entities = {
		@EntityResult(entityClass = PlafonMapping.class, fields = {
				@FieldResult(name = "id",column = "id"),
				@FieldResult(name = "tenor",column = "tenor"),
				@FieldResult(name = "plafon",column = "plafon"),
				@FieldResult(name = "monthlyInstallment",column = "monthly_installment"),
		}) })

@Entity
public class PlafonMapping {
	@Id
    private Long id;
    private Integer tenor;
    private BigDecimal plafon;
    private BigDecimal monthlyInstallment;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getTenor() {
		return tenor;
	}
	public void setTenor(Integer tenor) {
		this.tenor = tenor;
	}
	public BigDecimal getPlafon() {
		return plafon;
	}
	public void setPlafon(BigDecimal plafon) {
		this.plafon = plafon;
	}
	public BigDecimal getMonthlyInstallment() {
		return monthlyInstallment;
	}
	public void setMonthlyInstallment(BigDecimal monthlyInstallment) {
		this.monthlyInstallment = monthlyInstallment;
	}
    
    
}
