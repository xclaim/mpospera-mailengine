package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("ALL")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PMResponse extends BaseResponse {

    /* untuk addPM */
    private String pmId;
    private String localId;
    private List<ListSWResponse> swPlanList;
    /* untuk list PM */
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<ListPMResponse> pmList;
    private List<String> deletedPmList;

    public String getPmId() {
        return pmId;
    }

    public void setPmId(String pmId) {
        this.pmId = pmId;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<ListPMResponse> getPmList() {
        if (this.pmList == null)
            this.pmList = new ArrayList<>();
        return pmList;
    }

    public void setPmList(List<ListPMResponse> pmList) {
        this.pmList = pmList;
    }

    public List<ListSWResponse> getSwPlanList() {
        return swPlanList;
    }

    public void setSwPlanList(List<ListSWResponse> swPlanList) {
        this.swPlanList = swPlanList;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<String> getDeletedPmList() {
        return deletedPmList;
    }

    public void setDeletedPmList(List<String> deletedPmList) {
        this.deletedPmList = deletedPmList;
    }

}