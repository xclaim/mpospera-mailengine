package id.co.telkomsigma.btpns.mprospera.model.pm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;

@Entity
@Table(name = "T_PROJECTION_MEETING")
public class ProjectionMeeting extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 3336450864575228543L;
    private Long pmId;
    private String areaId;
    private String pmLocationName;
    private String pmOwner;
    private String pmPlanNumber;
    private String pmNumber;
    private String pmOwnerPhoneNumber;
    private String time;
    private Date pmDate;
    private Date pmPlanDate;
    private String longitude;
    private String latitude;
    private Date createdDate;
    private String createdBy;
    private String rrn;
    private Boolean isDeleted = false;
    private String areaName;
    private String status;
    private MiniMeeting mmId;
    private String swCandidate;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getPmId() {
        return pmId;
    }

    public void setPmId(Long pmId) {
        this.pmId = pmId;
    }

    @Column(name = "area_id", nullable = false)
    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    @Column(name = "location_name", nullable = false)
    public String getPmLocationName() {
        return pmLocationName;
    }

    public void setPmLocationName(String pmLocationName) {
        this.pmLocationName = pmLocationName;
    }

    @Column(name = "owner", nullable = false)
    public String getPmOwner() {
        return pmOwner;
    }

    public void setPmOwner(String pmOwner) {
        this.pmOwner = pmOwner;
    }

    @Column(name = "pm_number", nullable = true)
    public String getPmNumber() {
        return pmNumber;
    }

    public void setPmNumber(String pmNumber) {
        this.pmNumber = pmNumber;
    }

    @Column(name = "longitude", nullable = true)
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Column(name = "latitude", nullable = true)
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "created_date", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by", nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "is_deleted")
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Transient
    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Column(name = "phone_number")
    public String getPmOwnerPhoneNumber() {
        return pmOwnerPhoneNumber;
    }

    public void setPmOwnerPhoneNumber(String pmOwnerPhoneNumber) {
        this.pmOwnerPhoneNumber = pmOwnerPhoneNumber;
    }

    @Column(name = "time")
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Column(name = "pm_date")
    public Date getPmDate() {
        return pmDate;
    }

    public void setPmDate(Date pmDate) {
        this.pmDate = pmDate;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MiniMeeting.class)
    @JoinColumn(name = "mm_id", referencedColumnName = "id", nullable = true)
    public MiniMeeting getMmId() {
        return mmId;
    }

    public void setMmId(MiniMeeting mmId) {
        this.mmId = mmId;
    }

    @Column(name = "pm_plan_number")
    public String getPmPlanNumber() {
        return pmPlanNumber;
    }

    public void setPmPlanNumber(String pmPlanNumber) {
        this.pmPlanNumber = pmPlanNumber;
    }

    @Column(name = "sw_candidate")
    public String getSwCandidate() {
        return swCandidate;
    }

    public void setSwCandidate(String swCandidate) {
        this.swCandidate = swCandidate;
    }

    @Column(name = "pm_plan_date")
    public Date getPmPlanDate() {
        return pmPlanDate;
    }

    public void setPmPlanDate(Date pmPlanDate) {
        this.pmPlanDate = pmPlanDate;
    }

}