package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.*;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = "T_LOAN_PRODUCT")
public class LoanProduct extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long productId;
	private String prosperaId;
	private String productName;
	private Integer tenor;
	private Integer installmentCount;
	private String loanType;
	private String installmentFreqTime;
	private Integer installmentFreqCount;
	private String status;
	private BigDecimal margin;
	private String productCode;
	private BigDecimal productRate;
    private BigDecimal iir;
	private Date updateDate;
	private String jenisNasabah;
	private String tujuanpembiayaan;
	private String regularPiloting;
	private String typeNasabah;
	private Integer tenorBulan;
	

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "tenor")
	public Integer getTenor() {
		return tenor;
	}

	public void setTenor(Integer tenor) {
		this.tenor = tenor;
	}

	@Column(name = "installment_count")
	public Integer getInstallmentCount() {
		return installmentCount;
	}

	public void setInstallmentCount(Integer installmentCount) {
		this.installmentCount = installmentCount;
	}

	@Column(name = "loan_type")
	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	@Column(name = "installment_freq_time")
	public String getInstallmentFreqTime() {
		return installmentFreqTime;
	}

	public void setInstallmentFreqTime(String installmentFreqTime) {
		this.installmentFreqTime = installmentFreqTime;
	}

	@Column(name = "installment_freq_count")
	public Integer getInstallmentFreqCount() {
		return installmentFreqCount;
	}

	public void setInstallmentFreqCount(Integer installmentFreqCount) {
		this.installmentFreqCount = installmentFreqCount;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "margin")
	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	@Column(name = "prospera_id")
	public String getProsperaId() {
		return prosperaId;
	}

	public void setProsperaId(String prosperaId) {
		this.prosperaId = prosperaId;
	}
	
	@Column(name = "product_code")
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name = "product_rate")
	public BigDecimal getProductRate() {
		return productRate;
	}

	public void setProductRate(BigDecimal productRate) {
		this.productRate = productRate;
	}
	
    @Column(name = "iir")
    public BigDecimal getIir() {
        return iir;
    }

    public void setIir(BigDecimal iir) {
        this.iir = iir;
    }
	

	@Column(name = "update_date")
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "jenis_nasabah")
	public String getJenisNasabah() {
		return jenisNasabah;
	}

	public void setJenisNasabah(String jenisNasabah) {
		this.jenisNasabah = jenisNasabah;
	}

	

	@Column(name = "tujuan_pembiayaan")
	public String getTujuanpembiayaan() {
		return tujuanpembiayaan;
	}

	public void setTujuanpembiayaan(String tujuanpembiayaan) {
		this.tujuanpembiayaan = tujuanpembiayaan;
	}

	@Column(name = "regular_piloting")
	public String getRegularPiloting() {
		return regularPiloting;
	}

	public void setRegularPiloting(String regularPiloting) {
		this.regularPiloting = regularPiloting;
	}

	@Column(name = "type_nasabah")
	public String getTypeNasabah() {
		return typeNasabah;
	}

	public void setTypeNasabah(String typeNasabah) {
		this.typeNasabah = typeNasabah;
	}
	
	@Column(name = "tenor_bulan")
	public Integer getTenorBulan() {
		return tenorBulan;
	}

	public void setTenorBulan(Integer tenorBulan) {
		this.tenorBulan = tenorBulan;
	}


//	@OneToMany(fetch = FetchType.EAGER, targetEntity = DetailMappingProduct.class, cascade = CascadeType.ALL)
//	@JoinColumn(name = "product_id")
//	public Set<DetailMappingProduct> getDetailMappingProducts() {
//		return detailMappingProducts;
//	}
//
//	public void setDetailMappingProducts(Set<DetailMappingProduct> detailMappingProducts) {
//		this.detailMappingProducts = detailMappingProducts;
//	}



}
