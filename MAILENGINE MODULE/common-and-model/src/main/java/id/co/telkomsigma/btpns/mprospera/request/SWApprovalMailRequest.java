package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


public class SWApprovalMailRequest {

	private String username;
    private String swId;
    private String status;
    private String finalRecommendation;
    private String rejectedReason;
    private String userBWMP;
    private String localId;
    private List<SWApprovalHistoryReq> history;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSwId() {
		return swId;
	}
	public void setSwId(String swId) {
		this.swId = swId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFinalRecommendation() {
		return finalRecommendation;
	}
	public void setFinalRecommendation(String finalRecommendation) {
		this.finalRecommendation = finalRecommendation;
	}
	public String getRejectedReason() {
		return rejectedReason;
	}
	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}
	public String getUserBWMP() {
		return userBWMP;
	}
	public void setUserBWMP(String userBWMP) {
		this.userBWMP = userBWMP;
	}
	public String getLocalId() {
		return localId;
	}
	public void setLocalId(String localId) {
		this.localId = localId;
	}
	public List<SWApprovalHistoryReq> getHistory() {
		return history;
	}
	public void setHistory(List<SWApprovalHistoryReq> history) {
		this.history = history;
	}
    
    
}
