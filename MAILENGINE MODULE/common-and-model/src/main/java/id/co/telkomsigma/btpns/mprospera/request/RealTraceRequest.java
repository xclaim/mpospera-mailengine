package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class RealTraceRequest {

    private String detailMessage;
    private List<StackTraceRequest> stackTrace;
    private List<SuppressedException> suppressedExceptions;

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    public List<StackTraceRequest> getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(List<StackTraceRequest> stackTrace) {
        this.stackTrace = stackTrace;
    }

    public List<SuppressedException> getSuppressedExceptions() {
        return suppressedExceptions;
    }

    public void setSuppressedExceptions(List<SuppressedException> suppressedExceptions) {
        this.suppressedExceptions = suppressedExceptions;
    }

    @Override
    public String toString() {
        return "RealTraceRequest [detailMessage=" + detailMessage + ", stackTrace=" + stackTrace
                + ", suppressedExceptions=" + suppressedExceptions + "]";
    }

}