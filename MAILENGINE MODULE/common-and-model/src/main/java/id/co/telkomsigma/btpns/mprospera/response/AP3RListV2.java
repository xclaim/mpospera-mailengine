package id.co.telkomsigma.btpns.mprospera.response;

public class AP3RListV2 {

    private String ap3rId;
    private String productName;
    private String selectedPlafon;
    private String status;
    private String createdBy;
    private String customerName;
    private String cifNumber;
    private String address;
    private String sentraName;
    private String appId;

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSelectedPlafon() {
        return selectedPlafon;
    }

    public void setSelectedPlafon(String selectedPlafon) {
        this.selectedPlafon = selectedPlafon;
    }

}