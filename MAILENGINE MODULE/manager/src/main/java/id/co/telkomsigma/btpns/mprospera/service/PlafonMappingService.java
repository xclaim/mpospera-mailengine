package id.co.telkomsigma.btpns.mprospera.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.PlafonMappingManager;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;

@Service("plafonMappingService")
public class PlafonMappingService {
	
	@Autowired
	PlafonMappingManager plafonMappingManager;

	public PlafonMapping findByMonthlyInstallment(Integer installement) {
		return plafonMappingManager.findByMonthlyInstallment(installement);
	}
}
