package id.co.telkomsigma.btpns.mprospera.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import id.co.telkomsigma.btpns.mprospera.response.SWResponse;

@FeignClient("acqv2")
public interface Acqv2Interface {

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
	@RequestMapping(value="/acqv2/webservice/approvalSW",method = {RequestMethod.POST})
	SWResponse doApproval(@RequestBody String requestString);
}
