package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.feign.Acqv2Interface;
import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.response.SWResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("swService")
public class SWService extends GenericService {

    @Autowired
    @Qualifier("swManager")
    private SWManager swManager;

    @Autowired
    private UserManager userManager;
    
    @Autowired
    private Acqv2Interface acqv2Interface;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    
    public SWResponse approveSW(String request) {
    	return acqv2Interface.doApproval(request);
    }

    public Integer countAll() {
        return swManager.countAll();
    }

    public Page<SurveyWawancara> getSWByUser(String username, String page, String getCountData, String startDate,
                                             String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return swManager.findAllByUser(userIdList);
            else
                return swManager.findAllByUserPageable(userIdList,
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return swManager.findByCreatedDateAndUser(userIdList, formatter.parse(startDate), formatter.parse(endDate));
        else
            return swManager.findByCreatedDateAndUserPageable(userIdList, formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<SurveyWawancara> getSWByUserMS(String username, String page, String getCountData, String startDate,
                                               String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        User userSW = userManager.getUserByUsername(username);
        List<User> userListWisma = userManager.getUserByLocId(userSW.getOfficeCode());
        List<String> userIdList = new ArrayList<>();
        for (User user : userListWisma) {
            userIdList.add(user.getUsername());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return swManager.findAllByUserMS(userIdList);
            else
                return swManager.findAllByUserMSPageable(userIdList,
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return swManager.findByCreatedDateAndUserMS(userIdList, formatter.parse(startDate), formatter.parse(endDate));
        else
            return swManager.findByCreatedDateAndUserMSPageable(userIdList, formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<SurveyWawancara> getSWNonMS(String page, String getCountData, String startDate,
                                            String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return swManager.findAllNonMs();
            else
                return swManager.findAllNonMsPageable(
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return swManager.findByCreatedDateNonMs(formatter.parse(startDate), formatter.parse(endDate));
        else
            return swManager.findByCreatedDateNonMsPageable(formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<SurveyWawancara> getSWNonMSWithUserId(Long userId, String page, String getCountData, String startDate,
                                                      String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        int pageNumber = Integer.parseInt(page) - 1;
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return swManager.findAllNonMsWithUserId(userId);
            else
                return swManager.findAllNonMsWithUserIdPageable(userId,
                        new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return swManager.findByCreatedDateNonMs(formatter.parse(startDate), formatter.parse(endDate));
        else
            return swManager.findByCreatedDateNonMsPageable(formatter.parse(startDate),
                    formatter.parse(endDate), new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<LoanProduct> getProduct(String page, String getCountData) throws ParseException {
        if (getCountData == null)
            page = null;
        int pageNumber = Integer.parseInt(page) - 1;
        if (page == null || page.equals(""))
            return swManager.findAllProduct();
        else
            return swManager.findAllProductPageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<BusinessType> getBusinessType(String page, String getCountData) throws ParseException {
        if (getCountData == null)
            page = null;
        int pageNumber = Integer.parseInt(page) - 1;
        if (page == null || page.equals(""))
            return swManager.findAllBusinessType();
        else
            return swManager.findAllBusinessTypePageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Page<BusinessTypeAP3R> getBusinessTypeAp3r(String page, String getCountData) throws ParseException {
        if (getCountData == null)
            page = null;
        int pageNumber = Integer.parseInt(page) - 1;
        if (page == null || page.equals(""))
            return swManager.findAllBusinessTypeAP3R();
        else
            return swManager.findAllBusinessTypeAP3RPageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public Boolean isValidSw(String swId) {
        return swManager.isValidSw(swId);
    }

    public void save(SurveyWawancara surveyWawancara) {
        swManager.save(surveyWawancara);
    }

    public void save(SWUserBWMPMapping swUserBWMPMapping) {
        swManager.save(swUserBWMPMapping);
    }

    public SWUserBWMPMapping findBySwAndUser(Long sw, Long userId) {
        return swManager.findBySwAndUser(sw, userId);
    }

    public SWUserBWMPMapping findBySwAndLevel(Long sw, String level) {
        return swManager.findBySwAndLevel(sw, level);
    }

    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swManager.findBySw(sw);
    }

    public void save(SwIdPhoto swIdPhoto) {
        swManager.save(swIdPhoto);
    }

    public void save(SwSurveyPhoto swSurveyPhoto) {
        swManager.save(swSurveyPhoto);
    }

    public void save(DirectBuyThings directBuyProduct) {
        swManager.save(directBuyProduct);
    }

    public void save(AWGMBuyThings awgmBuyProduct) {
        swManager.save(awgmBuyProduct);
    }

    public void save(NeighborRecommendation neighbor) {
        swManager.save(neighbor);
    }

    public void save(OtherItemCalculation otherItemCalculation) {
        swManager.save(otherItemCalculation);
    }

    public void save(SWProductMapping map) {
        swManager.save(map);
    }

    public SwIdPhoto getSwIdPhoto(String swId) {
        return swManager.getSwIdPhoto(swId);
    }

    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        return swManager.getSwSurveyPhoto(swId);
    }

    public SurveyWawancara getSWById(String swId) {
        return swManager.getSWById(swId);
    }

    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        return swManager.findByProductId(productId);
    }

    public LoanProduct findByProductId(String productId) {
        return swManager.findByProductId(productId);
    }

    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        return swManager.findProductMapBySwId(swId);
    }

    public SWProductMapping findProductMapById(Long id) {
        return swManager.findProductMapById(id);
    }

    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        return swManager.findProductBySwId(sw);
    }

    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        return swManager.findAwgmProductBySwId(sw);
    }

    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        return swManager.findNeighborBySwId(sw);
    }

    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return swManager.findOtherItemCalcBySwId(swId);
    }

    public SurveyWawancara findSwByLocalId(String localId) {
        return swManager.findByLocalId(localId);
    }

    public void deleteDirectBuy(DirectBuyThings id) {
        swManager.deleteDirectBuy(id);
    }

    public void deleteAwgmBuy(AWGMBuyThings id) {
        swManager.deleteAwgmBuy(id);
    }

    public void deleteNeigbourReff(NeighborRecommendation id) {
        swManager.deleteReffNeighbour(id);
    }

    public void deleteOtherItemCalc(OtherItemCalculation id) {
        swManager.deleteOtherItemCalc(id);
    }

    public SWProductMapping findProductMapByLocalId(String localId) {
        return swManager.findProductMapByLocalId(localId);
    }

    public List<SurveyWawancara> findIsDeletedSwList() {
        return swManager.findIsDeletedSwList();
    }
    
    public List<SurveyWawancara> findSwByCustomer(Long customerId, String status){
    	return swManager.findSwByCustomerId(customerId, status);
    }

}