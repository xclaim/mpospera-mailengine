package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.UserEmail;

import java.util.List;


/**
 * Created by daniel on 3/31/15.
 */
public interface UserManager {

    /**
     * Gets single user by username
     *
     * @param username
     * @return user or null
     */
    User getUserByUsername(String username);

    User getUserByUserId(Long userId);

    List<User> getUserByLocId(String locId);

    List<User> findUserNonMS();
    
    
    /**
     * Gets single SessionKey by username
     *
     * @param username
     * @return user or null
     */
    String getSessionKeyByUsername(String username);
    
    User findByEmail(String email);

    void clearCache();

	List<UserEmail> findUserByLimit(long id);

}