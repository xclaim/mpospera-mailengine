package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.MenuDao;
import id.co.telkomsigma.btpns.mprospera.manager.MenuManager;
import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("menuManager")
public class MenuManagerImpl implements MenuManager {

    @Autowired
    private MenuDao menuDao;

    @Override
    public List<Menu> getAll() {
        return menuDao.findAll();
    }

    @Override
    public void clearCache() {
    }

}