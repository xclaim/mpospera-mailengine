package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalActivityManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by daniel on 3/31/15.
 */
@Service("terminalActivityManager")
public class TerminalActivityManagerImpl implements TerminalActivityManager {

    @Autowired
    TerminalActivityDao terminalActivityDao;

    @Override
    public TerminalActivity insertTerminalActivity(TerminalActivity terminalActivity) {

        terminalActivity = terminalActivityDao.save(terminalActivity);

        return terminalActivity;
    }

}