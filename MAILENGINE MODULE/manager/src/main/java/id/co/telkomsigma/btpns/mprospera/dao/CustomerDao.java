package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerDao extends JpaRepository<Customer, Long> {

    Integer countByAssignedUsername(String assignedUsername);

    Customer findTopBySwId(Long swId);

    Customer findTopByPdkId(Long pdkId);

    Customer findByRrn(String rrn);
    
    @Query(value = "select top 1 " + 
    		"SUM(lprs.installment_amt+lprs.current_margin) over (PARTITION by c.id) as angsuranTotal " + 
    		"FROM t_customer c " + 
    		"INNER JOIN t_loan_account la ON c.id=la.customer_id " + 
    		"CROSS APPLY(select top 1 lprs.id,lprs.due_date,lprs.overdue_days,lprs.created_date,lprs.installment_amt,lprs.current_margin " + 
    		"FROM t_loan_prs lprs WHERE lprs.loan_id = la.id ORDER by lprs.due_date DESC) lprs " + 
    		"where c.id=:custId and la.status='ACTIVE'",nativeQuery=true)
    Object findActiveInstallmentCost(@Param("custId") Long custId);

}