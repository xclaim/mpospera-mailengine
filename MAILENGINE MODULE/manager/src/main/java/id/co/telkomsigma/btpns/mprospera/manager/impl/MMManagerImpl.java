package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.dao.MMDao;
import id.co.telkomsigma.btpns.mprospera.manager.MMManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@SuppressWarnings("RedundantIfStatement")
@Service("mmManager")
public class MMManagerImpl implements MMManager {

    @Autowired
    private MMDao mmDao;

    @Override
    public int countAll() {
        return mmDao.countAll();
    }

    @Override
    public Page<MiniMeeting> findAll(List<String> kelIdList) {
        return mmDao.findByAreaIdIn(kelIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<MiniMeeting> findAllMM() {
        return mmDao.findAll(new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<MiniMeeting> findAllMMPageable(PageRequest pageRequest) {
        return mmDao.findAll(pageRequest);
    }

    @Override
    public Page<MiniMeeting> findAllByCreatedDate(Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDate(startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<MiniMeeting> findAllByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findAllByCreatedDate(startDate, cal.getTime(), pageRequest);
    }

    @Override
    public Page<MiniMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findByCreatedDate(startDate, cal.getTime(), kelIdList, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<MiniMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                       PageRequest pageRequest) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        cal.add(Calendar.DATE, 1);
        return mmDao.findByCreatedDate(startDate, cal.getTime(), kelIdList, pageRequest);
    }

    @Override
    public void save(MiniMeeting miniMeeting) {
        mmDao.save(miniMeeting);
    }

    @Override
    public Boolean isValidMm(String mmId) {
        Integer count = mmDao.countByMmId(Long.parseLong(mmId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    public MiniMeeting findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return mmDao.findByRrn(rrn);
    }

    @Override
    public List<MiniMeeting> findIsDeletedMmList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return mmDao.findIsDeletedMmId(dateBefore7Days, new Date());
    }

}