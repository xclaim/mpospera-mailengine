package id.co.telkomsigma.btpns.mprospera.service;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dto.MailDto;
import id.co.telkomsigma.btpns.mprospera.dto.RejectDto;
import id.co.telkomsigma.btpns.mprospera.dto.ValidationDataDto;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

@Service("mailSenderService")
public class MailSenderService extends GenericService{

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;
	
	@Autowired
	private  MailTokenService mailTokenService;

	public void sendEMail(MailDto mail, Boolean isForwarded,Date expiredDate, ValidationDataDto validationData) throws MessagingException, IOException {
		MimeMessage message = mailSender.createMimeMessage();

		String token = UUID.randomUUID().toString();
		MailToken mailToken = new MailToken();
		mailToken.setExpiredDate(expiredDate);
		mailToken.setIsExpired("0");
		mailToken.setToken(token);
		mailToken.setSwId(mail.getSwId());
		mailToken.setCreatedBy(mail.getUserName());
		mailToken.setCreatedDate(new Date());
		mailToken.setSendTo(mail.getTo());
		mailToken.setNote(validationData.getCatatan());
		mailToken.setStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
		mailTokenService.save(mailToken);

		Map<String, Object> mailData = castObjectToMap(validationData);
		
		// set true if you want to include an attachment or using in line image
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

		
		// Set variable for mail template
		Context context = new Context();
		context.setVariable("isForwarded", isForwarded);
		context.setVariable("token", token);
		context.setVariable("domain", mail.getDomain());
		context.setVariables(mailData);

		// Get mail template under resource folder
		String mailBody = templateEngine.process(WebGuiConstant.MAIL_TEMPLATE_PATH, context);
		
		// Set mail header and body
	
		String subject = mail.getSubject().replaceAll("\\bname\\b", validationData.getNamaNasabah());
		messageHelper.setTo(mail.getTo());
		messageHelper.setFrom(mail.getMailServer());
		messageHelper.setSubject(subject);
		messageHelper.setText(mailBody, true); // set true if you using html file as template
		mailSender.send(message);
	}
	
	public void sendNotificationMail(MailDto mail, String custName) throws MessagingException, IOException{
		MimeMessage message = mailSender.createMimeMessage();
		
		// set true if you want to include an attachment or using in line image
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

		// Get variable for mail template
		Context context = new Context();
		context.setVariable("name", mail.getReciverName());
		context.setVariable("custName", custName);
		// Get mail template under resource folder
		String mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_TEMPLATE_PATH, context);

		// Set mail header and body
		String subject = mail.getSubject().replaceAll("\\bname\\b", custName);
		messageHelper.setTo(mail.getTo());
		messageHelper.setFrom(mail.getMailServer());
		messageHelper.setSubject(subject);
		messageHelper.setText(mailBody, true); // set true if you using html file as template

		mailSender.send(message);
		
	}
	
	public void sendNotificationRejectMail(RejectDto mail,boolean isBM) throws MessagingException, IOException{
		MimeMessage message = mailSender.createMimeMessage();
		String mailBody;
		// set true if you want to include an attachment or using in line image
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

		// Get variable for mail template
		Context context = new Context();
		context.setVariable("custName", mail.getCustName());
		context.setVariable("appidNo", mail.getAppidNo());
		context.setVariable("sentra", mail.getSentra());
		context.setVariable("alasan", mail.getAlasan());
		if(isBM) {
			context.setVariable("mms", mail.getMms());
			mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_REJECT_SCORING_BM_TEMPLATE_PATH, context);
		}else {
			mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_REJECT_SCORING_TEMPLATE_PATH, context);
		}

		// Set mail header and body
		String subject = mail.getSubject().replaceAll("\\bname\\b", mail.getCustName());
		messageHelper.setTo(mail.getTo());
		messageHelper.setFrom(mail.getMailServer());
		messageHelper.setSubject(subject);
		messageHelper.setText(mailBody, true); // set true if you using html file as template

		mailSender.send(message);
		
	}
	
	public void sendNotificationReturnMail(MailDto mail, String mailFrom, String namaNasabah, String alasan) throws MessagingException, IOException{
		MimeMessage message = mailSender.createMimeMessage();
		
		// set true if you want to include an attachment or using in line image
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

		// Get variable for mail template
		Context context = new Context();
		context.setVariable("alasan", alasan);
		context.setVariable("namaNasabah", namaNasabah);
		context.setVariable("fromAddress", mailFrom);
		context.setVariable("username", mail.getReciverName());
		
		// Get mail template under resource folder
		String mailBody = templateEngine.process(WebGuiConstant.MAIL_NOTIF_RETURN_TEMPLATE_PATH, context);

		// Set mail header and body
		String subject = mail.getSubject().replaceAll("\\bname\\b", namaNasabah);
		messageHelper.setTo(mail.getTo());
		messageHelper.setFrom(mail.getMailServer());
		messageHelper.setSubject(subject);
		messageHelper.setText(mailBody, true); // set true if you using html file as template

		mailSender.send(message);
		
	}
	
	private Map<String,Object> castObjectToMap(Object object){
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(object, new TypeReference<Map <String, Object>>(){});
		
	}
	
	
}
