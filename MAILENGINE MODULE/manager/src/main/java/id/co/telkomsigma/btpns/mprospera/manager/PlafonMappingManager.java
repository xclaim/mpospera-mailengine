package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;

import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;



public interface PlafonMappingManager {
	
	PlafonMapping findByMonthlyInstallment(Integer installement);

}
