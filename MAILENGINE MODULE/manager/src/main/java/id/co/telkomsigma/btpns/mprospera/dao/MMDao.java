package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MMDao extends JpaRepository<MiniMeeting, Long> {

    @Query("SELECT COUNT(m) FROM MiniMeeting m")
    int countAll();

    @Query("SELECT m FROM MiniMeeting m WHERE m.isDeleted=false ORDER BY m.createdBy ASC")
    Page<MiniMeeting> findAll(Pageable pageable);

    @Query("SELECT m FROM MiniMeeting m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted=false ORDER BY m.createdBy ASC")
    Page<MiniMeeting> findAllByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                           Pageable pageable);

    @Query("SELECT m FROM MiniMeeting m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted=false AND m.areaId in :areaList ORDER BY m.createdBy ASC")
    Page<MiniMeeting> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                        @Param("areaList") List<String> kelIdList, Pageable pageable);

    Integer countByMmId(long mmId);

    Page<MiniMeeting> findByAreaIdIn(List<String> kelIdList, Pageable pageRequest);

    MiniMeeting findByRrn(String rrn);

    @Query("SELECT m FROM MiniMeeting m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate and m.isDeleted = true")
    List<MiniMeeting> findIsDeletedMmId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}