package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PmDao extends JpaRepository<ProjectionMeeting, String> {

    Integer countByPmId(long pmId);

    @Query("SELECT p FROM ProjectionMeeting p WHERE p.isDeleted=false")
    Page<ProjectionMeeting> getAllPm(Pageable pageRequest);

    @Query("SELECT p FROM ProjectionMeeting p WHERE p.createdDate>=:startDate AND p.createdDate<:endDate AND p.isDeleted=false")
    Page<ProjectionMeeting> findAllByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageRequest);

    @Query("SELECT p FROM ProjectionMeeting p WHERE p.createdDate>=:startDate AND p.createdDate<:endDate AND p.isDeleted=false AND p.areaId in :areaList")
    Page<ProjectionMeeting> findByCreatedDate(@Param("areaList") List<String> kelIdList,
                                              @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageRequest);

    ProjectionMeeting findByRrn(String rrn);

    List<ProjectionMeeting> findByMmId(MiniMeeting mmId);

    ProjectionMeeting findByPmId(Long pmId);

    @Query("SELECT p FROM ProjectionMeeting p WHERE p.createdDate>=:startDate AND p.createdDate<:endDate and p.isDeleted = true")
    List<ProjectionMeeting> findIsDeletedPmId(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}