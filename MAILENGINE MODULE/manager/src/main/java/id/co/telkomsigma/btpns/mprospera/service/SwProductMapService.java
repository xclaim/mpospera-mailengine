package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.SwProductMapManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;

@Service("swProductMapService")
public class SwProductMapService {

	@Autowired
	private SwProductMapManager swProductMapManager;
	
	public SWProductMapping findOneSwProductMapBySwId(Long id) {
		return swProductMapManager.findOneSwProductMapBySwId(id);
	}
}
