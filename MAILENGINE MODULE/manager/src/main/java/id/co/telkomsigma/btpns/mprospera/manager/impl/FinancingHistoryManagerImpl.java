package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.FinancingHistoryManager;
import id.co.telkomsigma.btpns.mprospera.pojo.FinancingHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.LoanPrs;
import id.co.telkomsigma.btpns.mprospera.pojo.MappingScoring;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.SwPreviousIncome;
import id.co.telkomsigma.btpns.mprospera.pojo.TotalAngsuran;

@Service("financingHistoryManager")
public class FinancingHistoryManagerImpl implements FinancingHistoryManager {

	@PersistenceContext
	private EntityManager em;
	

	private String mappingScoringQuery = "select id, transaksi_bailout,frekuensi_absen from t_mapping_scoring where application_id=:appId";	
	
	private String queryStr = "select top 3 loanAcc.id as id,loanAcc.application_id as application_id,loanAcc.plafond as plafond, "+
			"loanAcc.status as status, loanPro.product_name as product_name,loanPro.tenor_bulan as tenor_bulan " + 
			"from t_loan_account loanAcc " + 
			"join t_loan_product loanPro on loanAcc.product_id = loanPro.id " + 
			"where loanAcc.customer_id=:customerId and loanAcc.status in('ACTIVE','CLOSED') " + 
			"and loanPro.loan_type='TipePembiayaan-PembiayaanBaru' and loanPro.tujuan_pembiayaan='Modal Kerja' " + 
			"order by loanAcc.disbursement_date desc";
	
	private String queryTopupStr = "select top 1 loanAcc.id as id,loanAcc.application_id as application_id,loanAcc.plafond as plafond, " + 
			"loanAcc.status as status, loanPro.product_name as product_name,loanPro.tenor_bulan as tenor_bulan " + 
			"from t_loan_account loanAcc " + 
			"join t_loan_product loanPro on loanAcc.product_id = loanPro.id " + 
			"where loanAcc.customer_id=:customerId and loanAcc.status in('ACTIVE','CLOSED') " + 
			"and loanPro.loan_type='TipePembiayaan-TopUp' and loanPro.tujuan_pembiayaan='Modal Kerja' " + 
			"order by loanAcc.disbursement_date desc";
	
	private String queryKonsumtifStr = "select top 1 loanAcc.id as id,loanAcc.application_id as application_id,loanAcc.plafond as plafond, " + 
			"loanAcc.status as status, loanPro.product_name as product_name,loanPro.tenor_bulan as tenor_bulan " + 
			"from t_loan_account loanAcc " + 
			"join t_loan_product loanPro on loanAcc.product_id = loanPro.id " + 
			"where loanAcc.customer_id=:customerId and loanAcc.status in('ACTIVE','CLOSED') " + 
			"and loanPro.tujuan_pembiayaan='Konsumtif' " + 
			"order by loanAcc.disbursement_date desc";
	
	private String queryTotalAngsuran = "select id as id, (current_margin+installment_amt) as total_angsuran from t_loan_prs where loan_id in (select id from t_loan_account where customer_id=:customerId and status='ACTIVE')";
	
	private String queryPrsStr = "select top 1 loanPrs.id as id, loanPrs.outstanding_amt as outstanding_amt,loanPrs.current_margin as current_margin, " + 
			"loanPrs.installment_amt as installment_amt,loanPrs.overdue_days as overdue_days from t_loan_prs loanPrs " + 
			"where loanPrs.loan_id=:loanId order by created_date desc";
	
	private String queryPlafonStr = "select top 3 loanAcc.id as id,loanAcc.plafond as plafond "+
			"from t_loan_account loanAcc " + 
			"join t_loan_product loanPro on loanAcc.product_id = loanPro.id " + 
			"where loanAcc.customer_id=:customerId and loanAcc.status='ACTIVE' " + 
			"and loanPro.loan_type='TipePembiayaan-PembiayaanBaru' and loanPro.tujuan_pembiayaan='Modal Kerja' " + 
			"order by loanAcc.disbursement_date desc";
	
	private String queryPreviousIncomeStr = "select top 1 id, total_income from t_survey_wawancara where customer_id=:customerId and status='APPROVED' order by created_date desc";

	@SuppressWarnings("unchecked")
	@Override
	public List<MappingScoring> getMappingScoring(String appId) {
		Query query = this.em.createNativeQuery(mappingScoringQuery,"MappingScoring");
		query.setParameter("appId", appId);
		List<MappingScoring> mappingScoring = query.getResultList();
		em.close();
		return mappingScoring;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancingHistory> getFinancingHistory(Long customerId) {
		Query query = this.em.createNativeQuery(queryStr, "FinancingHistory");
		query.setParameter("customerId", customerId);
		List<FinancingHistory> historyList = query.getResultList();
		em.close();
		return historyList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancingHistory> getTopupHistory(Long customerId) {
		Query query = this.em.createNativeQuery(queryTopupStr, "FinancingHistory");
		query.setParameter("customerId", customerId);
		List<FinancingHistory> topup =  query.getResultList();
		em.close();
		return topup;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancingHistory> getKonsumtifHistory(Long customerId) {
		Query query = this.em.createNativeQuery(queryKonsumtifStr, "FinancingHistory");
		query.setParameter("customerId", customerId);
		List<FinancingHistory> konsumtif = query.getResultList();
		em.close();
		return konsumtif;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TotalAngsuran> getTotalAngsuran(Long customerId) {
		Query query = this.em.createNativeQuery(queryTotalAngsuran, "TotalAngsuran");
		query.setParameter("customerId", customerId);
		List<TotalAngsuran> totalAngsuranList = query.getResultList();
		em.close();
		return totalAngsuranList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LoanPrs> getLoanPrs(Long loanId) {
		Query query = this.em.createNativeQuery(queryPrsStr, "LoanPrs");
		query.setParameter("loanId", loanId);
		List<LoanPrs> loanPrsList = query.getResultList();
		em.close();
		return loanPrsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SwPreviousIncome> getPreviousIncome(Long customerId) {
		Query query = this.em.createNativeQuery(queryPreviousIncomeStr, "SwPreviousIncome");
		query.setParameter("customerId", customerId);
		List<SwPreviousIncome> income = query.getResultList();
		em.close();
		return income;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlafonHistory> getPlafonHistory(Long customerId) {
		Query query = this.em.createNativeQuery(queryPlafonStr, "PlafonHistory");
		query.setParameter("customerId", customerId);
		List<PlafonHistory> plafon = query.getResultList();
		em.close();
		return plafon;
	}

}
