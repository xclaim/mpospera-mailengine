package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.PmDao;
import id.co.telkomsigma.btpns.mprospera.manager.PmManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

@SuppressWarnings("RedundantIfStatement")
@Service("pmManager")
public class PmManagerImpl implements PmManager {

    @Autowired
    private PmDao pmDao;

    @Override
    @Caching(evict = {@CacheEvict(value = "wln.pm.isValidPm", key = "#pm.pmId", beforeInvocation = true),
            @CacheEvict(value = "wln.pm.findById", key = "#pm.pmId", beforeInvocation = true)})
    public void save(ProjectionMeeting pm) {
        pmDao.save(pm);
    }

    @Override
    @Cacheable(value = "wln.pm.isValidPm", unless = "#result == null")
    public Boolean isValidPm(String pmId) {
        Integer count = pmDao.countByPmId(Long.parseLong(pmId));
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public Page<ProjectionMeeting> getPm() {
        return pmDao.getAllPm(new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<ProjectionMeeting> getPmPageable(PageRequest pageRequest) {
        return pmDao.getAllPm(pageRequest);
    }

    @Override
    public Page<ProjectionMeeting> findPmByCreatedDate(Date startDate, Date endDate) {
        return pmDao.findAllByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<ProjectionMeeting> findPmByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest) {
        return pmDao.findAllByCreatedDate(startDate, endDate, pageRequest);
    }

    @Override
    public Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return pmDao.findByCreatedDate(kelIdList, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                             PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return pmDao.findByCreatedDate(kelIdList, startDate, endDate, pageRequest);
    }

    @Override
    @CacheEvict(value = {"wln.pm.findById", "wln.pm.isValidPm", "countAllPmNotDeleted", "getAllPm", "getAllPmPageable", "findPMByCreatedDate",
            "findPMByCreatedDatePageablePagable", "findPMByRrn", "wolverine.pm.isValidPm"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    public ProjectionMeeting findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return pmDao.findByRrn(rrn);
    }

    @Override
    public List<ProjectionMeeting> findByMmId(MiniMeeting mmId) {
        // TODO Auto-generated method stub
        return pmDao.findByMmId(mmId);
    }

    @Override
    @Cacheable(value = "wln.pm.findById", unless = "#result == null")
    public ProjectionMeeting findById(String pmId) {
        // TODO Auto-generated method stub
        return pmDao.findByPmId(Long.parseLong(pmId));
    }

    @Override
    public List<ProjectionMeeting> findIsDeletedPmList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return pmDao.findIsDeletedPmId(dateBefore7Days, new Date());
    }

}