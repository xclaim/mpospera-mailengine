package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.BusinessTypeDao;
import id.co.telkomsigma.btpns.mprospera.manager.BusinessTypeManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;

@Service("businessTypeManager")
public class BusinessTypeManagerImpl implements BusinessTypeManager {

	@Autowired
	private BusinessTypeDao businessTypeDao;
	
	@Override
	public BusinessType findByBusinessId(Long id) {
		return businessTypeDao.findByBusinessId(id);
	}

	@Override
	public List<BusinessType> findAll() {
		return businessTypeDao.findAll();
	}

	@Override
	public List<Long> findAllBusinessTypeId() {
		return businessTypeDao.findAllBusinessTypeId();
	}

}
