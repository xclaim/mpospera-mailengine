package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.OtherItemCalculation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Dzulfiqar on 30/05/2017.
 */
public interface OtherItemCalculationDao extends JpaRepository<OtherItemCalculation, Long> {

    List<OtherItemCalculation> findBySwId(Long swId);

}