package id.co.telkomsigma.btpns.mprospera.dao;


import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ManageApkDao extends JpaRepository<ManageApk, Long> {

    @Query("SELECT DISTINCT a FROM ManageApk a ORDER by a.createdDate DESC ")
    Page<ManageApk> getApk(Pageable pageable);

}
