package id.co.telkomsigma.btpns.mprospera.service;


import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("customerService")
public class CustomerService extends GenericService {

    @Autowired
    @Qualifier("customerManager")
    private CustomerManager customerManager;

    public void save(Customer customer) {
        customerManager.save(customer);
    }

    public Customer getBySwId(Long swId) {
        return customerManager.getBySwId(swId);
    }

    public Customer getByPdkId(Long pdkId) {
        return customerManager.getByPdkId(pdkId);
    }

    public Customer findById(String customerId) {
        if (customerId == null || "".equals(customerId))
            return null;
        return customerManager.findById(Long.parseLong(customerId));
    }
    
    public Customer findById(long customerId) {
    	return customerManager.findById(customerId);
    }

    public void delete(Customer customer) {
        // TODO Auto-generated method stub
        customerManager.delete(customer);
    }

    public Integer countAll(String username) {
        // TODO Auto-generated method stub
        return customerManager.countCustomerByUsername(username);
    }
    
    public BigDecimal findActiveInstallmentCost(Long custId) {
    	return customerManager.findActiveInstallmentCost(custId);
    }

}