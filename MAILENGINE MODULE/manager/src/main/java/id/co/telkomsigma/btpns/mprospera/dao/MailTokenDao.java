package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;



public interface MailTokenDao extends JpaRepository<MailToken, Long>  {

	MailToken findByToken(String token);
}
