package id.co.telkomsigma.btpns.mprospera.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.LoanProductManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

@Service("loanProductService")
public class LoanProductService {

	@Autowired
	private LoanProductManager loanProductManager;
	
	public LoanProduct findByProductId(Long id) {
		return loanProductManager.findByProductId(id);
	}
}
