package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.telkomsigma.btpns.mprospera.dao.MailTokenDao;
import id.co.telkomsigma.btpns.mprospera.manager.MailTokenManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

@Service("mailTokenManager")
public class MailTokenManagerImpl implements MailTokenManager{

	@Autowired
	MailTokenDao mailTokenDao;
	
	
	@Override
	public List<MailToken> findAll() {
		return mailTokenDao.findAll();
	}

	@Override
	public MailToken findByToken(String token) {
	
		return mailTokenDao.findByToken(token);
	}

	
	@Transactional
	@Override
	public MailToken save(MailToken mailToken) {
		mailToken = mailTokenDao.save(mailToken);
		return mailToken;
	}

}
