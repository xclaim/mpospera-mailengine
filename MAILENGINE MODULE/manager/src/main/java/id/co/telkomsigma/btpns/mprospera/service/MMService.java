package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.manager.MMManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("mmService")
public class MMService extends GenericService {

    @Autowired
    @Qualifier("mmManager")
    private MMManager mmManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public Integer countAll() {
        return mmManager.countAll();
    }

    public Page<MiniMeeting> getMM(String page, String getCountData, String startDate, String endDate, String username)
            throws ParseException {
        if (getCountData == null)
            page = null;
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if ((endDate == null || endDate.equals("")) && startDate != null)
            endDate = startDate;
        if (endDate != null && !endDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
        }
        if (startDate != null && !startDate.equals("")) {
            Calendar calStart = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            calStart.setTime(formatter.parse(startDate));
            calStart.add(Calendar.DATE, -7);
            startDate = formatter.format(calStart.getTime());
        }
        int pageNumber = Integer.parseInt(page) - 1;

        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return mmManager.findAllMM();
            else
                return mmManager.findAllMMPageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return mmManager.findAllByCreatedDate(formatter.parse(startDate), formatter.parse(endDate));
        else
            return mmManager.findAllByCreatedDatePageable(formatter.parse(startDate), formatter.parse(endDate),
                    new PageRequest(pageNumber, Integer.parseInt(getCountData)));
    }

    public void save(MiniMeeting miniMeeting) {
        mmManager.save(miniMeeting);
    }

    public Boolean isValidMm(String mmId) {
        return mmManager.isValidMm(mmId);
    }

    public MiniMeeting getMMByRrn(String rrn) {
        return mmManager.findByRrn(rrn);
    }

    public List<MiniMeeting> findIsDeletedMmList() {
        return mmManager.findIsDeletedMmList();
    }

}