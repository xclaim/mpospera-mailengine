package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.ProductPlafond;

public interface ProductPlafondDao extends JpaRepository<ProductPlafond, Long> {

    List<ProductPlafond> findByProductIdOrderByPlafond(LoanProduct productId);

}