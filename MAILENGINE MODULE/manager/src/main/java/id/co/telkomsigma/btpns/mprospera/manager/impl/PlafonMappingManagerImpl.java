package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;


import id.co.telkomsigma.btpns.mprospera.manager.PlafonMappingManager;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;


@Service("plafonMappingManager")
public class PlafonMappingManagerImpl implements PlafonMappingManager{

	@PersistenceContext
	EntityManager em;
	
	
	String plafonMappingQuery = "select id,tenor,plafon,monthly_installment from t_plafon_mapping where monthly_installment = :monthlyinstallment + (select min(abs(monthly_installment - :monthlyinstallment)) from t_plafon_mapping) or " 
	+ "monthly_installment = :monthlyinstallment - (select min(abs(monthly_installment - :monthlyinstallment)) from t_plafon_mapping)";
	
	
	@Override
	public PlafonMapping findByMonthlyInstallment(Integer installement) {
		Query query = this.em.createNativeQuery(plafonMappingQuery,"PlafonMapping");
		query.setParameter("monthlyinstallment", installement);
		PlafonMapping platform = (PlafonMapping) query.getSingleResult();

		em.close();
		return platform;
	}

}
