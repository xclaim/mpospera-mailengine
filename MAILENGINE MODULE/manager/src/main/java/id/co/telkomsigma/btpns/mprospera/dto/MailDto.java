package id.co.telkomsigma.btpns.mprospera.dto;

public class MailDto {
	private String mailServer;
	private String to;
	private String reciverName;
	private String subject;
	private String swId;
	private String userName;
	private String domain;


	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getReciverName() {
		return reciverName;
	}

	public void setReciverName(String reciverName) {
		this.reciverName = reciverName;
	}
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getMailServer() {
		return mailServer;
	}

	public void setMailServer(String mailServer) {
		this.mailServer = mailServer;
	}
	

	public String getSwId() {
		return swId;
	}

	public void setSwId(String swId) {
		this.swId = swId;
	}
	
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Override
	public String toString() {
	        return "Mail{" +
	                ", to='" + to + '\'' +
	                ", subject='" + subject + '\'' +
	                ", recieverName='" + reciverName + '\'' +
	                ", userName='" + userName + '\'' +
	                ", swId='" + swId + '\'' +
	                '}';
	 
	}
}
