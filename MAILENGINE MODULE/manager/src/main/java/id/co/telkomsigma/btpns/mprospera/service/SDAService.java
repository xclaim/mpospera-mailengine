package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.manager.SDAManager;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service("sdaService")
public class SDAService extends GenericService {

    @Autowired
    private AreaManager areaManager;

    @Autowired
    @Qualifier("sdaManager")
    private SDAManager sdaManager;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public AreaSubDistrict findSubDistrictById(String areaId) {
        return areaManager.findSubDistrictById(areaId);
    }

    public void saveSubDistrictDetails(AreaSubDistrictDetails areaDetails) throws Exception {
        sdaManager.saveSubDistrictDetails(areaDetails);
    }

    public void saveKelurahanDetails(AreaKelurahanDetails areaDetails) throws Exception {
        sdaManager.saveKelurahanDetails(areaDetails);
    }

    public Page<AreaSubDistrictDetails> getAreaSubDistrict(String page, String getCountData, String startDate,
                                                           String endDate, List<String> userList) throws Exception {
        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if ((endDate == null || endDate.equals("")) && startDate != null)
            endDate = startDate;
        if (endDate != null && !endDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
        }
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return sdaManager.getAllSubDistrictDetailsByCreatedBy(userList);
            else
                return sdaManager.getAllSubDistrictDetailsByCreatedBy(userList,
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return sdaManager.findByCreatedDate(userList, formatter.parse(startDate),
                    formatter.parse(endDate));
        else
            return sdaManager.findByCreatedDatePageable(userList, formatter.parse(startDate),
                    formatter.parse(endDate),
                    new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
    }

    public Boolean isValidSda(String sdaId, String type) {
        return sdaManager.isValidSda(sdaId, type);
    }

    public Integer countAll(AreaDistrict areaDistrictByUser) {
        return sdaManager.getAllSubDistrictDetailsByParentAreaId(areaDistrictByUser.getAreaId()).getSize();
    }

    public Page<AreaKelurahanDetails> getAreaKelurahan(String page, String getCountData, String startDate,
                                                       String endDate, List<String> userList) throws Exception {
        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if ((endDate == null || endDate.equals("")) && startDate != null)
            endDate = startDate;
        if (endDate != null && !endDate.equals("")) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            cal.setTime(formatter.parse(endDate));
            cal.add(Calendar.DATE, 1);
            endDate = formatter.format(cal.getTime());
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return sdaManager.getAllKelurahanDetailsByParentAreaId(userList);
            else
                return sdaManager.getAllKelurahanDetailsByParentAreaId(userList,
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return sdaManager.getAllKelurahanDetailsCreatedDate(userList, formatter.parse(startDate),
                    formatter.parse(endDate));
        else
            return sdaManager.getAllKelurahanDetailsCreatedDatePageable(userList, formatter.parse(startDate),
                    formatter.parse(endDate),
                    new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
    }

}