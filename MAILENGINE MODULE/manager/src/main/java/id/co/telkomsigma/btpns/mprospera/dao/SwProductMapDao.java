package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SwProductMapDao extends JpaRepository<SWProductMapping, Long> {

    @Query("SELECT s FROM SWProductMapping s WHERE s.swId = :swId AND s.deleted = false")
    List<SWProductMapping> findSwProductMapBySwId(@Param("swId") Long sw);
    
    @Query(value="select top 1 id,plafon,sw_id,product_id,rec_plafon,rec_product_id,is_deleted,local_id, installment, rec_installment from t_sw_product_map WHERE sw_id=:swId",nativeQuery=true)
    SWProductMapping findOneSwProductMapBySwId(@Param("swId") Long sw);

    SWProductMapping findByMappingId(Long id);

    SWProductMapping findByLocalId(String localId);

}