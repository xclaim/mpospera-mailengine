package id.co.telkomsigma.btpns.mprospera.dto;

public class RiwayatPembiayaanDto {
	
	private String produk;
	private String plafon;
	private String outstanding;
	private String angsuran;
	private String tenor;
	private String grade;
	private String status;
	private String dpd;
	
	
	public String getProduk() {
		return produk;
	}
	public void setProduk(String produk) {
		this.produk = produk;
	}
	public String getPlafon() {
		return plafon;
	}
	public void setPlafon(String plafon) {
		this.plafon = plafon;
	}
	public String getOutstanding() {
		return outstanding;
	}
	public void setOutstanding(String outstanding) {
		this.outstanding = outstanding;
	}
	public String getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(String angsuran) {
		this.angsuran = angsuran;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDpd() {
		return dpd;
	}
	public void setDpd(String dpd) {
		this.dpd = dpd;
	}


	

}
