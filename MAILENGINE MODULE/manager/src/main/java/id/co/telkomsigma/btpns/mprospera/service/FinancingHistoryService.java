package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.FinancingHistoryManager;
import id.co.telkomsigma.btpns.mprospera.pojo.FinancingHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.LoanPrs;
import id.co.telkomsigma.btpns.mprospera.pojo.MappingScoring;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.SwPreviousIncome;
import id.co.telkomsigma.btpns.mprospera.pojo.TotalAngsuran;

@Service("financingHistoryService")
public class FinancingHistoryService {
	
	@Autowired
	private FinancingHistoryManager financingHistoryManager;
	
	public List<MappingScoring> getMappingScoring(String appId) {
		return financingHistoryManager.getMappingScoring(appId);
	}
	
	public List<FinancingHistory> getFinancingHistory(Long customerId) {
		return financingHistoryManager.getFinancingHistory(customerId);
	}
	
	public List<FinancingHistory> getTopupHistory(Long customerId) {
		return financingHistoryManager.getTopupHistory(customerId);
	}
	
	public List<FinancingHistory> getKonsumtifHistory(Long customerId) {
		return financingHistoryManager.getKonsumtifHistory(customerId);
	}
	
	public List<TotalAngsuran> getTotalAngsuran(Long customerId) {
		return financingHistoryManager.getTotalAngsuran(customerId);
	}
	
	public List<LoanPrs> getLoanPrs(Long loanId) {
		return financingHistoryManager.getLoanPrs(loanId);
	}
	
	public List<SwPreviousIncome> getPreviousIncome(Long customerId) {
		return financingHistoryManager.getPreviousIncome(customerId);
	}
	
	public List<PlafonHistory> getPlafonHistory(Long customerId) {
		return financingHistoryManager.getPlafonHistory(customerId);
	}

}
