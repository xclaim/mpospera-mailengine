package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;

public interface SwProductMapManager {

	SWProductMapping findOneSwProductMapBySwId(Long id);
}
