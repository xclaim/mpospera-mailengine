package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

public interface LoanProductManager {
	
	LoanProduct findByProductId(Long id);

}
