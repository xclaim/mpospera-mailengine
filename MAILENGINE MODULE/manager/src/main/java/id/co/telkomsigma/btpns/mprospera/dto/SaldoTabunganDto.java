package id.co.telkomsigma.btpns.mprospera.dto;

public class SaldoTabunganDto {
	
	private String saatIni;
	private String akhirBulanLalu;
	private String akhirDuaBulanLalu;
	private String akhirTigaBulanLalu;
	
	public SaldoTabunganDto() {
	}
	
	public SaldoTabunganDto(String saatIni,String akhirBulanLalu,String akhirDuaBulanLalu,String akhirTigaBulanLalu) {
		this.saatIni = saatIni;
		this.akhirBulanLalu = akhirBulanLalu;
		this.akhirDuaBulanLalu = akhirDuaBulanLalu;
		this.akhirTigaBulanLalu = akhirTigaBulanLalu;
	}
	
	
	
	public String getSaatIni() {
		return saatIni;
	}
	public void setSaatIni(String saatIni) {
		this.saatIni = saatIni;
	}
	public String getAkhirBulanLalu() {
		return akhirBulanLalu;
	}
	public void setAkhirBulanLalu(String akhirBulanLalu) {
		this.akhirBulanLalu = akhirBulanLalu;
	}
	public String getAkhirDuaBulanLalu() {
		return akhirDuaBulanLalu;
	}
	public void setAkhirDuaBulanLalu(String akhirDuaBulanLalu) {
		this.akhirDuaBulanLalu = akhirDuaBulanLalu;
	}
	public String getAkhirTigaBulanLalu() {
		return akhirTigaBulanLalu;
	}
	public void setAkhirTigaBulanLalu(String akhirTigaBulanLalu) {
		this.akhirTigaBulanLalu = akhirTigaBulanLalu;
	}
	
	

}
