package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

public interface SentraManager {

    Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate);

    Page<Sentra> findByCreatedDatePageable(String username, String loc, Date startDate, Date endDate,
                                           PageRequest pageRequest);

    void clearCache();

    Sentra findByRrn(String rrn);

    List<Sentra> findAll();

}