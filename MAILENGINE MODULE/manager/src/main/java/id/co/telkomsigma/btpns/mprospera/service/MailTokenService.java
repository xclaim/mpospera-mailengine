package id.co.telkomsigma.btpns.mprospera.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.MailTokenManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

@Service("mailTokenService")
public class MailTokenService extends GenericService{

    @Autowired
    MailTokenManager mailTokenManager;
    
	public List<MailToken> findAll(){
		return mailTokenManager.findAll();
	}
	
	public MailToken findByToken (String token) {
		return mailTokenManager.findByToken(token);
	}
	
	public void save(MailToken mailToken) {
		mailTokenManager.save(mailToken);
	}	
}
