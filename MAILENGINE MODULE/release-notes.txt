prd version 5.10 - exception handling
----------------------------------------
Exception handling, 
Insurance claim, 
Uncompress partial service terkait upload foto, 
Duplikasi RRN pada message, 
Encoding UTF-8, 
Username null, 
Penambahan version info

prd version 5.9 - gzip compression
----------------------------------------
remove http hardcode
tiket:
PRB0040273-Gzip Compression
INC0106704-Nama Nasabah kosong, namun APPID muncul pada saat pelaksanaan PRS
PRB0040174-Nama Nasabah kosong, namun APPID muncul pada saat pelaksanaan PRS
INC0106704-Saldo WOW tidak terupdate di transaksi regular
-Laporan RMK m-PROSPERA isi pelunasan harusnya amount bukan jumlah trx
-SW yang sudah digunakan untuk deviasi dan deviasi telah disetujui masih dapat dipanggil untuk request deviasi
-Cetakan AP3R untuk mengupdate organisasi terbaru
Perubahan diisi oleh Bank:
Sebelumnya: "Pembina Sentra:" dan "Manager Sentra:" menjadi:
Nama:
Jabatan:
Tanggal:
PRB0040218-Pencairan pengganti hari libur, terbentuk appid dengan jadual angsuran yang tidak sesuai
PRB0040212-Penagihan di hari PRS tidak naik ke PROSPERA
INC0104956-Data pencairan dibawah tanggal prs, tidak berubah statusnya saat di cairkan
INC0107165-Filter Transaksi tidak naik ke PROSPERA
-Saldo Wowib tidak naik di tablet Mprospera tetapi naik di db mprospera
INC0106704-Foto Dokumen Pendukung tidak ada pada saat akan approve di Menu MS
PRB0040237-Label untuk jumlah persetujuan sebelum buka menu persetujuan terisi (0) namun berisi data, ketika dibuka beberapa appid status belum terupload (HO Approved)
PRB0040225-Sync list deviasi tab BM lambat
INC0139131-Ironman OutOfMemory