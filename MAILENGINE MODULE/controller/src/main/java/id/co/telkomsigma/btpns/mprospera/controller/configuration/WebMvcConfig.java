package id.co.telkomsigma.btpns.mprospera.controller.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.StandardTemplateModeHandlers;

import com.github.isrsal.logging.LoggingFilter;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;

/**
 * Created by daniel on 3/26/15.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Value("${default.locale.lang}")
    private String localeLanguage;

    @Value("${default.locale.country}")
    private String localeCountry;

    @Value("${default.timezone.lang}")
    private String timezoneLanguage;

    @Value("${default.timezone.country}")
    private String timezoneCountry;

    private static Locale language;
    private static Locale timezone;

    @Autowired
    ApplicationContext applicationContext;

    @Bean(name = "language")
    public Locale getLanguage() {
        if (language == null) {
            language = new Locale(localeLanguage, localeCountry);
        }
        return language;
    }

    @Bean(name = "timezone")
    public Locale getTimezone() {
        if (timezone == null) {
            timezone = new Locale(timezoneLanguage, timezoneCountry);
        }
        return timezone;
    }

    @Bean(name = "localeResolver")
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(getLanguage());
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/fonts/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/fonts/");
        registry.addResourceHandler("/img/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/img/");
        registry.addResourceHandler("/js/**").setCachePeriod(Integer.MAX_VALUE)
                .addResourceLocations("classpath:/static/js/");
    }

    @Bean
    public Filter logFilter() {
        return new LoggingFilter();
    }
    
    @Bean
    public SpringTemplateEngine springTemplateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(htmlTemplateResolver());
        return templateEngine;
    }

    @Bean
    public SpringResourceTemplateResolver htmlTemplateResolver(){
        SpringResourceTemplateResolver emailTemplateResolver = new SpringResourceTemplateResolver();
        emailTemplateResolver.setApplicationContext(applicationContext);
        emailTemplateResolver.setPrefix("/WEB-INF/view/");
        emailTemplateResolver.setSuffix(".html");
        emailTemplateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
        emailTemplateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        emailTemplateResolver.setCacheable(true);
        return emailTemplateResolver;
    }    

}