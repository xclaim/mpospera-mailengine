package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.dto.ApprovalVariable;
import id.co.telkomsigma.btpns.mprospera.dto.FrekwensiTransaksiDto;
import id.co.telkomsigma.btpns.mprospera.dto.MailDto;
import id.co.telkomsigma.btpns.mprospera.dto.RejectDto;
import id.co.telkomsigma.btpns.mprospera.dto.RiwayatPembiayaanDto;
import id.co.telkomsigma.btpns.mprospera.dto.SaldoTabunganDto;
import id.co.telkomsigma.btpns.mprospera.dto.ValidationDataDto;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;
import id.co.telkomsigma.btpns.mprospera.model.sw.BusinessType;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWUserBWMPMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.FinancingHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.LoanPrs;
import id.co.telkomsigma.btpns.mprospera.pojo.MappingScoring;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonHistory;
import id.co.telkomsigma.btpns.mprospera.pojo.PlafonMapping;
import id.co.telkomsigma.btpns.mprospera.pojo.SwPreviousIncome;
import id.co.telkomsigma.btpns.mprospera.pojo.TotalAngsuran;
import id.co.telkomsigma.btpns.mprospera.pojo.UserEmail;
import id.co.telkomsigma.btpns.mprospera.request.MailRequest;
import id.co.telkomsigma.btpns.mprospera.request.NotifRequest;
import id.co.telkomsigma.btpns.mprospera.request.RejectRequest;
import id.co.telkomsigma.btpns.mprospera.request.SWApprovalHistoryReq;
import id.co.telkomsigma.btpns.mprospera.request.SWApprovalMailRequest;
import id.co.telkomsigma.btpns.mprospera.response.MailResponse;
import id.co.telkomsigma.btpns.mprospera.response.SWResponse;
import id.co.telkomsigma.btpns.mprospera.service.BusinessTypeService;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.service.FinancingHistoryService;
import id.co.telkomsigma.btpns.mprospera.service.LoanProductService;
import id.co.telkomsigma.btpns.mprospera.service.LocationService;
import id.co.telkomsigma.btpns.mprospera.service.MailParameterService;
import id.co.telkomsigma.btpns.mprospera.service.MailSenderService;
import id.co.telkomsigma.btpns.mprospera.service.MailTokenService;
import id.co.telkomsigma.btpns.mprospera.service.PlafonMappingService;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.SwProductMapService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

@Controller("webserviceSendMailController")
public class SendMailController extends GenericController {

	@Autowired
	private MailParameterService mailParamSvc;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private TerminalActivityService terminalActivityService;

	@Autowired
	private TerminalService terminalService;

	@Autowired
	private MailSenderService mailSenderService;
	
	@Autowired
	private  MailTokenService mailTokenService;
	
	@Autowired
	private SWService sWService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BusinessTypeService businessTypeService;
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private SwProductMapService swProductMapService;
	
	@Autowired
	private LoanProductService loanProductService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private PlafonMappingService plafonMappingService;
	
	@Autowired
	private FinancingHistoryService financingHistoryService;

	JsonUtils jsonUtils = new JsonUtils();
	
	@Value("${mail.server}")
	String mailServer;
	
	@RequestMapping(value = WebGuiConstant.SEND_EMAIL_REQUEST, method = { RequestMethod.POST })
	public @ResponseBody MailResponse sendMail(@RequestBody final MailRequest request) {
		MailResponse response = new MailResponse();
		response.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
				log.info("INCOMING MESSAGE : " + jsonUtils.toJson(request));
				if(!isValidEmail(request.getEmailAddress())) {
					log.error("email address for username "+request.getApprovalUsername()+" is invalid");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,WebGuiConstant.MAIL_INVALID_ADDRESS);
				}
				HashMap<String, String> mailParamHash = getMailParam();
				ValidationDataDto validationData = setValidationData(request.getSwId());
				validationData.setApprovalUsername(request.getApprovalUsername());
				validationData.setCatatan(request.getNote());
				Date expiredDate = setExpiredDate(mailParamHash.get(WebGuiConstant.MAIL_DATE_FORMAT),mailParamHash.get(WebGuiConstant.MAIL_DATE_EXPIRED));

				String subject;
				if (request.getIsForwarded()) {
					subject = mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_FORWARDED);
				} else {
					subject = mailParamHash.get(WebGuiConstant.MAIL_SUBJECT);
				}

				MailDto mail = mailPack(request.getSwId(),subject,mailServer,request.getEmailAddress(),request.getUserName(),mailParamHash.get(WebGuiConstant.MAIL_DOMAIN));
				try {
					mailSenderService.sendEMail(mail, request.getIsForwarded(), expiredDate, validationData);
					log.info("mail send to "+request.getEmailAddress());
					response.setResponseMessage("send mail success");
				} catch (MessagingException | IOException ex) {
					log.error(ex.getMessage());
                    ex.printStackTrace();
                    setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,ex.getMessage());
				}

		} catch (Exception e) {
			log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,e.getMessage());
		} 
		finally {
			try {
				// insert MessageLogs
				log.info("Trying to CREATE Terminal Activity...");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
						terminalActivity.setSessionKey("");
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername("System");
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
						log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
						log.info("Updating Terminal Activity");
					}
				});
				log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
			} catch (Exception e) {
				log.error("ERROR CREATE terminalActivity : " + e.getMessage());
			}
			
		}
		return response;
	}

	@RequestMapping(value = WebGuiConstant.SEND_NOTIF_REQUEST, method = { RequestMethod.POST })
	public @ResponseBody MailResponse sendNotif(@RequestBody final NotifRequest request) {
		MailResponse response = new MailResponse();
		response.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
				log.info("INCOMING MESSAGE : " + jsonUtils.toJson(request));
				if(!isValidEmail(request.getEmailAddress())) {
					log.error("email address for username "+request.getUserName()+" is invalid");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,WebGuiConstant.MAIL_INVALID_ADDRESS);
				}

				HashMap<String,String> mailParamHash = getMailParam();
				MailDto mail = mailPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), request.getEmailAddress(), mailServer, request.getRecieverName());
				try {
					mailSenderService.sendNotificationMail(mail, request.getCustomerName());
					log.info("Notification send to "+request.getEmailAddress());
					response.setResponseMessage("send notif mail success");
				}catch (MessagingException | IOException ex) {
					log.error(ex.getMessage());
                    ex.printStackTrace();
                    setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,ex.getMessage());
				}
			
		} catch (Exception e) {
			log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,e.getMessage());
		} 
		finally {
			try {
				// insert MessageLogs
				log.info("Trying to CREATE Terminal Activity...");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
						terminalActivity.setSessionKey("");
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername("System");
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
						log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
						log.info("Updating Terminal Activity");
					}
				});
				log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
			} catch (Exception e) {
				log.error("ERROR CREATE terminalActivity : " + e.getMessage());
			}
			
		}
		return response;
	}
	
	@RequestMapping(value = WebGuiConstant.SEND_NOTIF_REJECT_SCORING_REQUEST, method = { RequestMethod.POST })
	public @ResponseBody MailResponse sendNotifReject(@RequestBody final RejectRequest request) {
		MailResponse response = new MailResponse();
		response.setResponseCode(WebGuiConstant.RC_SUCCESS);
		try {
				User user =  userService.findUserByUsername(request.getUserName());
				if(user == null) {
					log.error("username not found");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,WebGuiConstant.MAIL_USERNAME_NOT_FOUND);
				}
				boolean isBm = checkBmRole(user.getRoleUser());
				log.info("INCOMING MESSAGE : " + jsonUtils.toJson(request));
				if(!isValidEmail(user.getEmail())) {
					log.error("email address for username "+request.getUserName()+" is invalid");
					return getGeneralErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,WebGuiConstant.MAIL_INVALID_ADDRESS);
				}

				RejectDto mail = new RejectDto();
				HashMap<String,String> mailParamHash = getMailParam();
				if(isBm) {
					mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer,  request.getCustomerName(), request.getAppidNo(), request.getSentra(), request.getAlasan());
					mail.setMms(request.getMms());
				}else {
					mail = mailRejectPack(mailParamHash.get(WebGuiConstant.MAIL_SUBJECT_NOTIF), user.getEmail(), mailServer,  request.getCustomerName(), request.getAppidNo(), request.getSentra(), request.getAlasan());
				}
				try {
					mailSenderService.sendNotificationRejectMail(mail, isBm);
					log.info("Reject Notification send to "+user.getEmail());
					response.setResponseMessage("send notif mail success");
				}catch (MessagingException | IOException ex) {
					log.error(ex.getMessage());
                    ex.printStackTrace();
                    setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,ex.getMessage());
				}
			
		} catch (Exception e) {
			log.error(e.getMessage());
            e.printStackTrace();
            setErrorResponse(response,WebGuiConstant.RC_GENERAL_ERROR,e.getMessage());
		} 
		finally {
			try {
				// insert MessageLogs
				log.info("Trying to CREATE Terminal Activity...");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
						TerminalActivity terminalActivity = new TerminalActivity();
						// Logging to terminal activity
						terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
						terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SEND_MAIL);
						terminalActivity.setCreatedDate(new Date());
						terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
						terminalActivity.setSessionKey("");
						terminalActivity.setTerminal(terminal);
						terminalActivity.setUsername("System");
						List<MessageLogs> messageLogs = new ArrayList<>();
						terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
						log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
						log.info("Updating Terminal Activity");
					}
				});
				log.info("RESPONSE MESSAGE : " + jsonUtils.toJson(response));
			} catch (Exception e) {
				log.error("ERROR CREATE terminalActivity : " + e.getMessage());
			}
			
		}
		return response;
	}
	
	@RequestMapping(value="/webservice/landingPage", method = RequestMethod.GET)
	public String landingPage(@RequestParam(name = "token") String token,
			@RequestParam(name = "status")String status,
			@RequestParam( name = "username") String username,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response){
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			
			//check if token is not found
			if(mailToken == null) {
				return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_TOKEN, model);
			}
			
			//check if request is already approved / rejected
			SurveyWawancara sw = sWService.getSWById(mailToken.getSwId());
			if(isAlreadyDecided(sw.getStatus())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,sw.getStatus());
				return getAlreadyDecidedErrorPage(sw.getStatus(),sw.getUpdatedBy(),model);
			}
			
			//check if token is expired
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
				//begin action
				if(status.equals("1")) {
					model.addAttribute("custName",sw.getCustomerName());
					model.addAttribute("token",token);
					model.addAttribute("approvalUsername",username);
					return WebGuiConstant.MAIL_APPROVE_CONFIRM_PATH;
				} else if(status.equals("2")) {
					model.addAttribute("token",token);
					model.addAttribute("username",username);
					return WebGuiConstant.MAIL_REJECT_REASON_PATH; //rejection
				} else if(status.equals("3")) {
					long swId = Long.parseLong(mailToken.getSwId());
					List<UserEmail> userList = userService.findUserByLimit(swId);
					HashMap<String,String> emails = new HashMap<String,String>();
					for(UserEmail user : userList) {
						emails.put(user.getUserLogin(), user.getEmail());
					}
					model.addAttribute("emails",emails);
					model.addAttribute("token",token);
					return WebGuiConstant.MAIL_FORWARD_PATH; //forward
				} else {
					return getGeneralErrorPage(WebGuiConstant.MAIL_UNKNOWN_STATUS,model);
				}
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
		
		
	}
	
	@RequestMapping(value="/webservice/approveLandingPage", method= RequestMethod.GET)
	public String approvalLandingPage(@RequestParam(name="username") String username, @RequestParam(name="token") String token,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
				List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()));
				SWResponse sWResponse = sWService.approveSW(approvalRequestPack(WebGuiConstant.STATUS_APPROVED, mailToken.getSwId(), username,listHistory));
				
				if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
					setUsedToken(mailToken,username,WebGuiConstant.STATUS_APPROVED);
					return WebGuiConstant.MAIL_SUCCESS_PAGE_PATH; //approved
				}else if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
					return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
				}else {
					return getGeneralErrorPage(sWResponse.getResponseMessage(),model);
				}
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
		
		
	}
	
	@RequestMapping(value="/webservice/forwardLandingPage", method = {RequestMethod.POST})
	public String forwardLandingPage(@RequestParam(value="email") String email,@RequestParam(name = "token") String token,
			final Model model, final HttpServletRequest request,final HttpServletResponse response) {
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())){
				boolean isForwarded = false;

				String[] mailArray = email.split(",");
				String mailAddress = mailArray[0];
				String username = mailArray[1];
				
				if(!isValidEmail(mailAddress)) {
					log.error("email address for username "+username+" is invalid");
					return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_ADDRESS, model);
				}
				List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()));
                SWResponse sWResponse = sWService.approveSW(approvalRequestPack(WebGuiConstant.STATUS_WAITING_APPROVAL, mailToken.getSwId(), username,listHistory));
                
				if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
					HashMap<String, String> mailParamHash = getMailParam();
					ValidationDataDto validationData = setValidationData(mailToken.getSwId());
					validationData.setApprovalUsername(username);
					validationData.setCatatan(mailToken.getNote());
					Date expiredDate = setExpiredDate(mailParamHash.get(WebGuiConstant.MAIL_DATE_FORMAT),mailParamHash.get(WebGuiConstant.MAIL_DATE_EXPIRED));
					MailDto mail = mailPack(mailToken.getSwId(), mailParamHash.get(WebGuiConstant.MAIL_SUBJECT), mailServer, mailAddress, username, mailParamHash.get(WebGuiConstant.MAIL_DOMAIN));
					try {
						mailSenderService.sendEMail(mail, isForwarded, expiredDate, validationData);
						log.info("Mail forwarded to "+mailAddress);
					}catch (MessagingException | IOException ex) {
						log.error(ex.getMessage());
						ex.printStackTrace();
						return getGeneralErrorPage(ex.getMessage(),model);
					}
					setUsedToken(mailToken,username,WebGuiConstant.STATUS_FORWARDED);
					model.addAttribute("email",mailAddress);
					return WebGuiConstant.MAIL_FWD_SUCCESS_PAGE_PATH;
				}else if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
					return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
				}else {
					return getGeneralErrorPage(sWResponse.getResponseMessage(),model);
				}
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
	}
	
	@RequestMapping(value="/webservice/returnReasonPage", method= RequestMethod.GET)
	public String returnReasonPage(@RequestParam(name="namaNasabah") String namaNasabah,
			@RequestParam(name = "token") String token,@RequestParam( name = "username") String username,
			final Model model, final HttpServletRequest request,final HttpServletResponse response) {
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			if(mailToken == null) {
				return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_TOKEN,model);
			}			
			
			//check if request is already approved / rejected
			SurveyWawancara sw = sWService.getSWById(mailToken.getSwId());
			if(isAlreadyDecided(sw.getStatus())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,sw.getStatus());
				return getAlreadyDecidedErrorPage(sw.getStatus(),sw.getUpdatedBy(),model);
			}
			
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
				User user = userService.findUserByUsername(mailToken.getCreatedBy());
				model.addAttribute("returnAddress", user.getEmail());
				model.addAttribute("token",token);
				model.addAttribute("fromAddress",mailToken.getSendTo());
				model.addAttribute("username",username);
				model.addAttribute("namaNasabah",namaNasabah);
				return WebGuiConstant.MAIL_RETURN_REASON_PATH;
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
	
		
	}
			
	@RequestMapping(value="/webservice/returnLandingPage", method = {RequestMethod.POST})
	public String returnLandingPage(@RequestParam(name = "fromAddress") String fromAddress,@RequestParam(name = "alasan") String alasan,
			@RequestParam(name="returnAddress") String returnAddress,@RequestParam(name="namaNasabah") String namaNasabah,
			@RequestParam(name = "token") String token,@RequestParam( name = "username") String username,
			final Model model, final HttpServletRequest request,final HttpServletResponse response) {
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())){

				if(!isValidEmail(returnAddress)) {
					log.error("email address for username "+username+" is invalid");
					return getGeneralErrorPage(WebGuiConstant.MAIL_INVALID_ADDRESS, model);
				}
				
				List<SWApprovalHistoryReq> listHistory = getUserBwmpMap(Long.parseLong(mailToken.getSwId()));
				SWResponse sWResponse = sWService.approveSW(approvalRequestPack(WebGuiConstant.STATUS_WAITING_APPROVAL, mailToken.getSwId(), username,listHistory));
				
				if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
					HashMap<String,String> param = getMailParam();
					User user = userService.findUserByUsername(mailToken.getCreatedBy());
					MailDto mailDto = mailPack(param.get(WebGuiConstant.MAIL_SUBJECT_RETURN), returnAddress, mailServer, user.getName());
					try {
						mailSenderService.sendNotificationReturnMail(mailDto, fromAddress, namaNasabah, alasan);
						log.info("Mail Returned to "+returnAddress);
					}catch (MessagingException | IOException ex) {
						log.error(ex.getMessage());
						ex.printStackTrace();
						return getGeneralErrorPage(ex.getMessage(),model);
					}
					setUsedToken(mailToken,username,WebGuiConstant.STATUS_RETURNED);
					model.addAttribute("namaNasabah",namaNasabah);
					model.addAttribute("returnAddress",returnAddress);
					return WebGuiConstant.MAIL_RETURN_PATH;
				}else if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
					return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
				}else {
					return getGeneralErrorPage(sWResponse.getResponseMessage(),model);
				}
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
			
		}catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
	}
	
	@RequestMapping(value="/webservice/rejectLandingPage", method= {RequestMethod.POST})
	public String rejectLandingPage(@RequestParam(name="token") String token,
			@RequestParam(name="alasanpenolakan") String alasanPenolakan,
			@RequestParam(name="username") String username,
			final Model model, final HttpServletRequest request,final HttpServletResponse response) {
		try {
			MailToken mailToken = mailTokenService.findByToken(token);
			if(mailToken.getIsExpired().equals("0") && !isDateExpired(mailToken.getExpiredDate())) {
                SWResponse sWResponse = sWService.approveSW(rejectRequestPack(mailToken.getSwId(), username,alasanPenolakan));
                
				if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_SUCCESS)) {
					setUsedToken(mailToken,username,WebGuiConstant.STATUS_REJECTED);
					model.addAttribute("alasanpenolakan", alasanPenolakan);
					return WebGuiConstant.MAIL_REJECT_SUCCESS_PATH;
				}else if(sWResponse.getResponseCode().equals(WebGuiConstant.RC_UNKNOWN_PHOTO_SW_ID)) {
					return WebGuiConstant.MAIL_ERROR_IMG_NOT_PROCESSED_PATH;
				}else {
					return getGeneralErrorPage(sWResponse.getResponseMessage(),model);
				}
			}else if(mailToken.getIsExpired().equals("0") && isDateExpired(mailToken.getExpiredDate())) {
				setUsedToken(mailToken,WebGuiConstant.MAIL_SYSTEM,WebGuiConstant.STATUS_EXPIRED);
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}else {
				return getGeneralErrorPage(WebGuiConstant.MAIL_TOKEN_EXPIRED,model);
			}
		}catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			return getGeneralErrorPage(e.getMessage(),model);
		}
	}
	
	/**
	 * Set all required variable for email body
	 * @param swId swId
	 * @return all required variable for email body
	 * @author kusnendi
	 */
	private ValidationDataDto setValidationData(String swId) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		ValidationDataDto val = new ValidationDataDto();
		
		//Chained query
		SurveyWawancara sw = sWService.getSWById(swId);
		//flag untuk mengecek apakah totalIncome nya 0
		boolean isZeroTotalIncome = checkZeroTotalIncome(sw.getTotalIncome());
		boolean isZeroPbu = false;
		boolean isNewCustomer = checkNewCustomer(sw.getCustomerRegistrationType().toString());
		
		User user = userService.findUserByUsername(sw.getCreatedBy());
		Location location = locationService.findByLocationId(user.getOfficeCode());
		SWProductMapping sWProductMapping = swProductMapService.findOneSwProductMapBySwId(Long.valueOf(swId));
		LoanProduct loanProduct = loanProductService.findByProductId(sWProductMapping.getRecommendedProductId());
		
		/*
		 * Jika pbu di bawah 2 jt (total income nya 0) 
		 * maka cari pbu sebelumnya yang total income nya tidak 0 dan telah di approve,
		 * Jika tidak ada maka set semua field yang membutuhkan perhitungan pembagian menjadi 0
		 * Agar terhindar dari pembagian dengan nol 
		 */
		
		if(isZeroTotalIncome) {
			if(!isNewCustomer) {
				List<SurveyWawancara> swList = sWService.findSwByCustomer(sw.getCustomerId(),WebGuiConstant.STATUS_APPROVED );
				if(!swList.isEmpty()) {
					boolean isNotZero = false;
					for(SurveyWawancara survey : swList) {
						if(survey.getTotalIncome().compareTo(BigDecimal.ZERO)==0) {
							continue;
						}else {
							isNotZero = true;
							sw = survey;
							sWProductMapping = swProductMapService.findOneSwProductMapBySwId(sw.getSwId());
							loanProduct = loanProductService.findByProductId(sWProductMapping.getRecommendedProductId());
							break;
						}
					}
					if(!isNotZero) {
						isZeroPbu = true;
					}
				}else {
					isZeroPbu = true;
				}
			}else {
				isZeroPbu = true;
			}
			
		}

		
		//query parameter
		List<Long> businessTypeIds = businessTypeService.findAllBusinessTypeId();
		
		//custom query
		List<TotalAngsuran> totalAngsuranList = new ArrayList<TotalAngsuran>();
		List<SwPreviousIncome> previousIncomeList = new ArrayList<SwPreviousIncome>();
		List<PlafonHistory> plafonHistory = new ArrayList<PlafonHistory>();
		
		if(!isNewCustomer) {
			totalAngsuranList = financingHistoryService.getTotalAngsuran(sw.getCustomerId());
			previousIncomeList = financingHistoryService.getPreviousIncome(sw.getCustomerId());
			plafonHistory = financingHistoryService.getPlafonHistory(sw.getCustomerId());
		}
		
		// Fill email data begin
		
		/*
		 * Miscellaneous / Parameter
		 */
		val.setKategoriNasabah(sw.getCustomerRegistrationType().toString());
		ApprovalVariable appVar = new ApprovalVariable();
		BigDecimal angsuranTotal = BigDecimal.ZERO;
		if(isNewCustomer) {
			appVar = countVariable(sw);
		}else {
			appVar = countVariable(sw,sw.getCustomerId());
			angsuranTotal = countAngsuranTotal(totalAngsuranList);
		}
		
		
		
		/*
		 * Header
		 */
		val.setNamaSco(user.getName());
		val.setNamaNasabah(sw.getCustomerName());
		val.setNamaMms(location.getName());
		val.setKodeMms(location.getLocationCode());
		if(isNewCustomer) {
			val.setTahunKe("0");
		}else {
			Customer customer = customerService.findById(sw.getCustomerId());
			val.setTahunKe(compareYears(customer.getCreatedDate()));
		}
		
		/*
		 * Pengajuan Pembiayaan
		 */
		val.setProduk(loanProduct.getProductName());
		val.setPlafon(formatCurrency(sWProductMapping.getRecommendedPlafon()));
		val.setTenor(loanProduct.getTenorBulan().toString());
		if(sWProductMapping.getRecommendedInstallment() != null) {
			val.setAngsuran(formatCurrency(sWProductMapping.getRecommendedInstallment()));
		}else {
			val.setAngsuran(formatCurrency(0.0));
		}
		
		val.setAngsuranTotal(formatCurrency(angsuranTotal));
		val.setMargin(formatPercent(loanProduct.getMargin()));
		if(sw.getDisbursementDate() != null) {
			val.setTglCair(sdf.format(sw.getDisbursementDate()));
		}else {
			val.setTglCair("");
		}
		
		/*
		 * Limit Pembiayaan
		 */
		PlafonMapping plafonMapping = plafonMappingService.findByMonthlyInstallment(appVar.getMaxAngsuran().intValue());
		val.setLimitPembiayaanTotal(formatCurrency(plafonMapping.getPlafon()));
		val.setLimitTerpakai(formatCurrency(angsuranTotal));
		val.setLimitTersisa(formatCurrency(plafonMapping.getPlafon().subtract(angsuranTotal)));
		
		/*
		 * Rasio
		 */
		if(isNewCustomer) {
			val.setTotalPlafonAktif(formatPercent(0.0));
			val.setTotalSaldoTabungan(formatPercent(0.0));
		}else {
			if(isZeroPbu) {
				val.setTotalPlafonAktif(formatPercent(0.0));
				val.setTotalSaldoTabungan(formatPercent(0.0));
			}else {
				val.setTotalPlafonAktif(formatPercent(countTotalActivePlafon(plafonHistory, sWProductMapping, sw)));
				val.setTotalSaldoTabungan(formatPercent(0.0)); //tabungan not available
			}
			
		}
		
		/*
		 * Riwayat Pembiayaan - Pembiayaan Utama dan Tambahan
		 */
		if(!isNewCustomer) {
			List<FinancingHistory> historyList = financingHistoryService.getFinancingHistory(sw.getCustomerId());
			List<FinancingHistory> topup = financingHistoryService.getTopupHistory(sw.getCustomerId());
			List<FinancingHistory> konsumtif = financingHistoryService.getKonsumtifHistory(sw.getCustomerId());

			//Riwayat Pembiayaan Utama
			
			//Isi riwayat lain nya dengan 0 jika riwayat pembayaran hanya ada 1 atau 2
			if(historyList.size() == 1) {
				val.setBiayaSblmAkhir(getDefaultFinancingHistory());
				val.setBiayaSebelumY(getDefaultFinancingHistory());
			}else if(historyList.size() == 2) {
				val.setBiayaSebelumY(getDefaultFinancingHistory());
			}
			
			//pengisian kolom riwayat pembiayaan utama
			int i = 0;
			for(FinancingHistory history : historyList ) {
				RiwayatPembiayaanDto riwayatTerakhir = setFinancingHistory(history);
				if(i==0) {
					val.setBiayaAkhir(riwayatTerakhir);
				}else if(i==1) {
					val.setBiayaSblmAkhir(riwayatTerakhir);
				}else {
					val.setBiayaSebelumY(riwayatTerakhir);
				}
				i++;
			}
			
			//riwayat topup
			if(!topup.isEmpty()) {
				for (FinancingHistory topups : topup) {
					val.setBiayaTopup(setFinancingHistory(topups));
				}
			}else {
				val.setBiayaTopup(getDefaultFinancingHistory());
			}
			
			//rowayat konsumtif
			if(!konsumtif.isEmpty()) {
				for(FinancingHistory konsumtifs : konsumtif) {
					val.setBiayaKonsumtif(setFinancingHistory(konsumtifs));
				}
			}else {
				val.setBiayaKonsumtif(getDefaultFinancingHistory());
			}	
		}
		
		/*
		 * Riwayat Tabungan (to be Confirm )
		 */
		val.setSaldoTabungan(getDefaultSavingBalance());
		val.setFrek(getDefaultTransactionFrek());
		
		/*
		 * Profil Usaha
		 */
		if(businessTypeIds.contains(Long.parseLong(sw.getBusinessField()))) {
			BusinessType businessType = businessTypeService.findByBusinessId(Long.valueOf(sw.getBusinessField()));
			val.setJenisUsaha(businessType.getBusinessType());
		}else {
			val.setJenisUsaha(" ");
		}
		val.setDeskripsiUsaha(sw.getBusinessDescription());
		val.setWaktuOperasional(sw.getBusinessDaysOperation());
		val.setStatusTempatUsaha(setStatusTemptUsaha(sw.getBusinessWorkStatus().toString()));
		val.setLamaUsaha(String.valueOf((sw.getBusinessAgeYear()*12)+sw.getBusinessAgeMonth()));
		
		/*
		 * Perhitungan Pendapatan
		 */
		val.setPendapatanPenjualan(formatCurrency(sw.getTotalIncome()));
		val.setPembelian(formatCurrency(sw.getTotalBuy()));
		val.setPengeluaranUsaha(formatCurrency(sw.getBusinessCost()));
		val.setPendapatanBersih(formatCurrency(appVar.getPendapatanBersih()));
		val.setPendapatanLainnya(formatCurrency(sw.getOtherIncome()));
		val.setPengeluaranNonUsaha(formatCurrency(sw.getNonBusinessCost()));
		val.setSisaPenghasilan(formatCurrency(appVar.getPendapatanBersih().subtract(sw.getNonBusinessCost())));
		val.setMaxAngsuran(formatCurrency(appVar.getMaxAngsuran()));
		val.setMaxAngsuranPersen(formatPercent(appVar.getMaxAngsuranPercent()));
		val.setIir(loanProduct.getIir().toString());
		val.setIirNominal("0"); //unused

		/*
		 * Pendapatan Sebelumnya
		 */
		if(!previousIncomeList.isEmpty()) {
			SwPreviousIncome previousIncome = previousIncomeList.get(0);
			if(sw.getTotalIncome().equals(previousIncome.getTotalIncome())) {
				val.setIncomeDataIsSame(true);
				val.setKenaikanPendapatan(formatPercent(0.0));
				val.setPenurunanPendapatan(formatPercent(0.0));
			}else {
				if(isZeroPbu) {
					val.setIncomeDataIsSame(true);
					val.setKenaikanPendapatan(formatPercent(0.0));
					val.setPenurunanPendapatan(formatPercent(0.0));
				}else {
					val.setIncomeDataIsSame(false);
					if(sw.getTotalIncome().compareTo(previousIncome.getTotalIncome()) == 1) {
						val.setKenaikanPendapatan(formatPercent(countKenaikanPendapatan(previousIncome, sw)));
						val.setPenurunanPendapatan(formatPercent(0.0));
					}else {
						val.setKenaikanPendapatan(formatPercent(0.0));
						val.setPenurunanPendapatan(formatPercent(countPenurunanPendapatan(previousIncome, sw)));
					}
				}

			}
		}else {
			val.setIncomeDataIsSame(true);
			val.setKenaikanPendapatan(formatPercent(0.0));
			val.setPenurunanPendapatan(formatPercent(0.0));
		}
		
		// fill email data end
		return val;
	}
	
	/**
	 * Check if total income is zero
	 * @param totalIncome total income
	 * @return true if total income is zero
	 * @author kusnendi
	 */
	private boolean checkZeroTotalIncome(BigDecimal totalIncome) {
		if(totalIncome.compareTo(BigDecimal.ZERO)==0) {
			return true;
		}else {
			return false;
		}
	}
	
	private boolean checkNewCustomer(String customerType) {
		if(customerType.equals("1")) {
			return true;
		}else {
			return false;
		}
	}
	
	private String setStatusTemptUsaha(String statusTempatUsaha) {
		if(statusTempatUsaha.equals("1")) {
			return WebGuiConstant.MILIK_PRIBADI;
		}else if(statusTempatUsaha.equals("2")) {
			return WebGuiConstant.MILIK_ORANG_TUA;
		}else if(statusTempatUsaha.equals("3")) {
			return WebGuiConstant.MILIK_PIHAK_KETIGA;
		}else {
			return WebGuiConstant.MILIK_LAINNYA;
		}
	}
	
	private BigDecimal countKenaikanPendapatan(SwPreviousIncome previousIncome,SurveyWawancara sw) {
		BigDecimal kenaikan = sw.getTotalIncome().divide(previousIncome.getTotalIncome(),4,RoundingMode.HALF_UP);
		return kenaikan.multiply(new BigDecimal(100));
	}
	
	private BigDecimal countPenurunanPendapatan(SwPreviousIncome previousIncome,SurveyWawancara sw) {
		BigDecimal penurunan = previousIncome.getTotalIncome().divide(sw.getTotalIncome(),4,RoundingMode.HALF_UP);
		return penurunan.multiply(new BigDecimal(100));
	}
	
	private BigDecimal countTotalActivePlafon(List<PlafonHistory> plafonHistory,SWProductMapping sWProductMapping,SurveyWawancara sw) {
		BigDecimal plafonHistoryTotal = BigDecimal.ZERO;
		if(!plafonHistory.isEmpty()) {
			for(PlafonHistory plafon : plafonHistory) {
				plafonHistoryTotal = plafonHistoryTotal.add(plafon.getPlafond());
			}
		}
		BigDecimal totalPlafornAktif = sWProductMapping.getRecommendedPlafon().add(plafonHistoryTotal);
		BigDecimal pendapatanPenjualan = sw.getTotalIncome().multiply(new BigDecimal(12));
		return (totalPlafornAktif.divide(pendapatanPenjualan, 4, RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
	}
	
	private ApprovalVariable countVariable(SurveyWawancara sw,long custId) {
		BigDecimal installmentCost = BigDecimal.ZERO;
		if(sw.getInstallmentCost() != null) {
			installmentCost = sw.getInstallmentCost();
		}else {
			BigDecimal instTemp = customerService.findActiveInstallmentCost(custId);
			if(instTemp != null) {
				installmentCost = instTemp;
			}else {
				installmentCost = BigDecimal.ZERO;
			}
		}
		
		BigDecimal pendapatanBersih = sw.getTotalIncome().subtract(sw.getExpenditure());
		BigDecimal angsuran = pendapatanBersih.multiply(new BigDecimal(0.4));
		BigDecimal maxAngsuran = angsuran.subtract(installmentCost);
		BigDecimal maxAngsuranPersen = BigDecimal.ZERO;
			if(pendapatanBersih.compareTo(BigDecimal.ZERO) == 0) {
				maxAngsuranPersen = BigDecimal.ZERO;
			}else {
				maxAngsuranPersen = (installmentCost.divide(pendapatanBersih,4,RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
			}
		return new ApprovalVariable(sw.getExpenditure(),pendapatanBersih,angsuran,maxAngsuran,maxAngsuranPersen);
	}
	
	private ApprovalVariable countVariable(SurveyWawancara sw) {
		BigDecimal installmentCost = BigDecimal.ZERO;
		if(sw.getInstallmentCost() != null) {
			installmentCost = sw.getInstallmentCost();
		}
		BigDecimal pendapatanBersih = sw.getTotalIncome().subtract(sw.getExpenditure());
		BigDecimal angsuran = pendapatanBersih.multiply(new BigDecimal(0.4));
		BigDecimal maxAngsuran = angsuran.subtract(installmentCost);
		BigDecimal maxAngsuranPersen = BigDecimal.ZERO;
		if(pendapatanBersih.compareTo(BigDecimal.ZERO) == 0) {
			maxAngsuranPersen = BigDecimal.ZERO;
		}else {
			maxAngsuranPersen = (installmentCost.divide(pendapatanBersih,4,RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
		}
		return new ApprovalVariable(sw.getExpenditure(),pendapatanBersih,angsuran,maxAngsuran,maxAngsuranPersen);
	}
	
	private BigDecimal countAngsuranTotal(List<TotalAngsuran> totalAngsuranList){
		BigDecimal angsuranTotal = BigDecimal.ZERO;
		for(TotalAngsuran totalAngsuran : totalAngsuranList) {
			angsuranTotal = angsuranTotal.add(totalAngsuran.getTotalAngsuran());
		}
		return angsuranTotal;
	}
	
	private RiwayatPembiayaanDto setFinancingHistory(FinancingHistory financingHistory) {
		List<MappingScoring> mappingScoring = financingHistoryService.getMappingScoring(financingHistory.getAppId());
		List<LoanPrs> loanPrsList = financingHistoryService.getLoanPrs(financingHistory.getLoanId());
		RiwayatPembiayaanDto riwayat = new RiwayatPembiayaanDto();
		riwayat.setPlafon(formatCurrency(financingHistory.getPlafond()));
		riwayat.setStatus(financingHistory.getStatus());
		if(!loanPrsList.isEmpty()) {
			for(LoanPrs loanPrs : loanPrsList) {
				riwayat.setOutstanding(formatCurrency(loanPrs.getOutstandingAmount()));
				riwayat.setAngsuran(formatCurrency(loanPrs.getCurrentMargin().add(loanPrs.getInstallmentAmount())));
				riwayat.setDpd(loanPrs.getOverdueDays().toString());
			}
		}else {
			riwayat.setOutstanding(formatCurrency(0.0));
			riwayat.setAngsuran(formatCurrency(0.0));
			riwayat.setDpd("0");
		}
		riwayat.setTenor(financingHistory.getTenorBulan().toString());
		riwayat.setProduk(financingHistory.getProductName());
		if(!mappingScoring.isEmpty()) {
			for(MappingScoring score : mappingScoring) {
				riwayat.setGrade(getGrade(financingHistory.getTenorBulan(),score.getFrekuensiAbsen(),score.getTransaksiBaliout()));
			}
		}else {
			riwayat.setGrade("Tidak ada data grade");
		}
		return riwayat;
	}
	
	private String getGrade(int tenor, int absensiPrs, int danaSolidaritas) {
		if(tenor<12) {
			return "Belum di Tentukan";
		}else if(tenor == 12) {
			if(absensiPrs<=4) {
				if(danaSolidaritas <=2) {
					return "A";
				}else if(danaSolidaritas > 2 && danaSolidaritas <=4) {
					return "B";
				}else if(danaSolidaritas == 5) {
					return "C";
				}else if(danaSolidaritas > 5) {
					return "D";
				}else {
					return "Undefinied";
				}
			} else if(absensiPrs >4 && absensiPrs <=9){
				if(danaSolidaritas <=2) {
					return "B";
				}else if(danaSolidaritas > 2 && danaSolidaritas <=4) {
					return "B";
				}else if(danaSolidaritas == 5) {
					return "C";
				}else if(danaSolidaritas > 5) {
					return "D";
				}else {
					return "Undefinied";
				}
			} else if(absensiPrs == 10) {
				if(danaSolidaritas <=2) {
					return "B";
				}else if(danaSolidaritas > 2 && danaSolidaritas <=4) {
					return "C";
				}else if(danaSolidaritas == 5) {
					return "D";
				}else if(danaSolidaritas > 5) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs > 10) {
				return "D";
			}else {
				return "Undefinied";
			}
		}else if(tenor > 12 && tenor <= 18) {
			if(absensiPrs<=6) {
				if(danaSolidaritas <=3) {
					return "A";
				}else if(danaSolidaritas > 3 && danaSolidaritas <=6) {
					return "B";
				}else if(danaSolidaritas == 7) {
					return "C";
				}else if(danaSolidaritas > 7) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs >6 && absensiPrs <=14) {
				if(danaSolidaritas <=3) {
					return "B";
				}else if(danaSolidaritas > 3 && danaSolidaritas <=6) {
					return "B";
				}else if(danaSolidaritas == 7) {
					return "C";
				}else if(danaSolidaritas > 7) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs == 15) {
				if(danaSolidaritas <=3) {
					return "B";
				}else if(danaSolidaritas > 3 && danaSolidaritas <=6) {
					return "C";
				}else if(danaSolidaritas == 7) {
					return "D";
				}else if(danaSolidaritas > 7) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs > 15) {
				return "D";
			}else {
				return "Undefinied";
			}
		}else if(tenor > 18 && tenor <= 24) {
			if(absensiPrs <= 9) {
				if(danaSolidaritas <=4) {
					return "A";
				}else if(danaSolidaritas > 4 && danaSolidaritas <=9) {
					return "B";
				}else if(danaSolidaritas == 10) {
					return "C";
				}else if(danaSolidaritas > 10) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs >9 && absensiPrs <=19) {
				if(danaSolidaritas <=4) {
					return "B";
				}else if(danaSolidaritas > 4 && danaSolidaritas <=9) {
					return "B";
				}else if(danaSolidaritas == 10) {
					return "C";
				}else if(danaSolidaritas > 10) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs == 20) {
				if(danaSolidaritas <=4) {
					return "B";
				}else if(danaSolidaritas > 4 && danaSolidaritas <=9) {
					return "C";
				}else if(danaSolidaritas == 10) {
					return "D";
				}else if(danaSolidaritas > 10) {
					return "D";
				}else {
					return "Undefinied";
				}
			}else if(absensiPrs > 20) {
				return "D";
			}else {
				return "Undefinied";
			}
		}else {
			return "Undefinied";
		}
	}
	
	/**
	 * Check if request is already approved / rejected / returned
	 * @param status status of the request
	 * @return whatever the request is already approved or not
	 * @author kusnendi
	 */
	private boolean isAlreadyDecided(String status) {
		if(status.equals(WebGuiConstant.STATUS_WAITING_APPROVAL)) {
			return false;
		}else {
			return true;
		}
		
	}
	
	/**
	 * Error page for already approved / rejected / returned request
	 * @param swStatus request status
	 * @param modifiedBy user who previously approved / rejected request
	 * @param model for adding attribute
	 * @return already decided error page path
	 * @author kusnendi
	 */
	private String getAlreadyDecidedErrorPage(String swStatus,String modifiedBy, Model model) {
		if(swStatus.equals(WebGuiConstant.STATUS_APPROVED)) {
			model.addAttribute("statusSw","Approve");
		}else if(swStatus.equals(WebGuiConstant.STATUS_REJECTED)) {
			model.addAttribute("statusSw","Reject");
		}else if(swStatus.equals(WebGuiConstant.STATUS_RETURNED)) {
			model.addAttribute("statusSw","Kembalikan");
		}else {
			model.addAttribute("statusSw",swStatus);
		}
		model.addAttribute("modifiedBy",modifiedBy);
		return WebGuiConstant.MAIL_ERROR_ALREADY_APPROVED_PATH;
	}
	
	/**
	 * Display general error page
	 * @param errorMessage message displayed on error page
	 * @param model for adding attribute
	 * @return general error page path
	 * @author kusnendi
	 */
	private String getGeneralErrorPage(String errorMessage, Model model) {
		model.addAttribute("errorMsg",errorMessage);
		return WebGuiConstant.MAIL_ERROR_GENERAL_PAGE_PATH;
    }
    
    /**
     * Return error response 
     * @param response response object
     * @param responseCode error code 
     * @param responseMessage error message
     * @return Error response
     * @author kusnendi
     */
    private MailResponse getGeneralErrorResponse(MailResponse response, String responseCode,String responseMessage){
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        return response;
    }

    /**
     * Set error response without return response object
     * @param response response object
     * @param responseCode error code
     * @param responseMessage error message
     * @author kusnendi
     */
    private void setErrorResponse(MailResponse response, String responseCode,String responseMessage){
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
    }
	
	/**
	 * Make used token expired
	 * @param mailToken token object
	 * @param modifiedBy user who set token expired
	 * @param statusSw status of the token
	 * @author kusnendi
	 */
	private void setUsedToken(MailToken mailToken,String modifiedBy,String statusSw) {
		mailToken.setIsExpired("1");
		mailToken.setModifiedBy(modifiedBy);
		mailToken.setModifiedDate(new Date());
		mailToken.setStatus(statusSw);
		mailTokenService.save(mailToken);
	}
	
	/**
	 * Pack all variable for approval request
	 * @param status approval status
	 * @param swId customer swId
	 * @param username approval username
	 * @param listHistory customer approval history
	 * @return SWApprovalMailRequest object
	 * @author kusnendi
	 * @throws IOException 
	 * @throws JsonGenerationException 
	 * @throws JsonMappingException 
	 */
	private String approvalRequestPack(String status, String swId, String username,List<SWApprovalHistoryReq> listHistory) throws JsonMappingException, JsonGenerationException, IOException {
		SWApprovalMailRequest approvalRequest= new SWApprovalMailRequest();
		approvalRequest.setStatus(status);
		approvalRequest.setSwId(swId);
		approvalRequest.setUsername(username);
        approvalRequest.setHistory(listHistory);
        String approvalRequestStr = new JsonUtils().toJson(approvalRequest);
		return approvalRequestStr;
	}
	
	/**
	 * Pack all variable for reject request
	 * @param swId customer swId
	 * @param username approval username
	 * @param rejectReason reason why this request rejected
	 * @return SWApprovalMailRequest object
	 * @author kusnendi
	 * @throws IOException 
	 * @throws JsonGenerationException 
	 * @throws JsonMappingException 
	 */
	private String rejectRequestPack(String swId, String username,String rejectReason) throws JsonMappingException, JsonGenerationException, IOException {
		SWApprovalMailRequest approvalRequest= new SWApprovalMailRequest();
		approvalRequest.setStatus(WebGuiConstant.STATUS_REJECTED);
		approvalRequest.setSwId(swId);
		approvalRequest.setUsername(username);
        approvalRequest.setRejectedReason(rejectReason);
        String approvalRequestStr = new JsonUtils().toJson(approvalRequest);
		return approvalRequestStr;
	}
	
	/**
	 * Pack all request for sending mail
	 * @param swId customer swId
	 * @param subject mail subject
	 * @param mailServerStr address of mail server
	 * @param emailAddress address of mail receiver 
	 * @param userName co username 
	 * @param mailDomain domain used for approval links
	 * @return MailDto object
	 * @author kusnendi
	 */
	private MailDto mailPack(String swId,String subject,String mailServerStr,String emailAddress,String userName,String mailDomain) {
		MailDto mail = new MailDto();
		mail.setSwId(swId);
		mail.setSubject(subject);
		mail.setMailServer(mailServerStr);
		mail.setTo(emailAddress);
		mail.setUserName(userName);
		mail.setDomain(mailDomain);
		return mail;
	}
	
	/**
	 * Pack all request for sending notification mail
	 * @param subject mail subject
	 * @param emailAddress  address of mail receiver 
	 * @param mailServerStr address of mail server
	 * @param recieverName name of mail receiver
	 * @return MailDto object
     * @author kusnendi
	 */
	private MailDto mailPack(String subject,String emailAddress,String mailServerStr,String recieverName) {
		MailDto mail = new MailDto();
		mail.setSubject(subject);
		mail.setTo(emailAddress);
		mail.setMailServer(mailServerStr);
		mail.setReciverName(recieverName);
		return mail;
	}
    
    /**
     * Pack all request for sending notification mail if request rejecred by system
     * @param subject mail subject
     * @param emailAddress reciever email address
     * @param mailServerStr sender email address
     * @param custName customer name
     * @param appidNo app Id number
     * @param sentra sentra
     * @param alasan reason why request rejected by system
     * @return RejectDto object
     * @author kusnendi
     */
	private RejectDto mailRejectPack(String subject,String emailAddress,String mailServerStr,String custName, String appidNo,String sentra,String alasan) {
		RejectDto reject = new RejectDto();
		reject.setSubject(subject);
		reject.setTo(emailAddress);
		reject.setMailServer(mailServerStr);
		reject.setCustName(custName);
		reject.setAppidNo(appidNo);
		reject.setSentra(sentra);
		reject.setAlasan(alasan);
		return reject;
	}	
	
	/**
	 * get default saving balance value
	 * @return default saving balance value
	 * @author kusnendi
	 */
	private SaldoTabunganDto getDefaultSavingBalance() {
		return new SaldoTabunganDto("0","0","0","0");
	}
	
	/**
	 * get default financing history value
	 * @return default financing history value
	 * @author kusnendi
	 */
	private RiwayatPembiayaanDto getDefaultFinancingHistory() {
		RiwayatPembiayaanDto riwayat1 = new RiwayatPembiayaanDto();
		riwayat1.setAngsuran(formatCurrency(0.0));
		riwayat1.setGrade("-");
		riwayat1.setOutstanding(formatCurrency(0.0));
		riwayat1.setPlafon(formatCurrency(0.0));
		riwayat1.setProduk("-");
		riwayat1.setStatus("-");
		riwayat1.setTenor("-");
		riwayat1.setDpd("0");
		return riwayat1;
	}
	
	/**
	 * get default transaction frequency value
	 * @return default transaction frequency value
	 * @author kusnendi
	 */
	private FrekwensiTransaksiDto getDefaultTransactionFrek() {
		return new FrekwensiTransaksiDto("0","0","0","0","0","0","0","0");
	}
	
	/**
	 * Get customer history and map to SWApprovalHistoryReq based of customer swId
	 * @param swId customer swId
	 * @return customer history, return null if not found
	 */
	private List<SWApprovalHistoryReq> getUserBwmpMap(Long swId) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		List<SWUserBWMPMapping> mapList = sWService.findBySw(swId);
		List<SWApprovalHistoryReq> historyList = new ArrayList<SWApprovalHistoryReq>();
		if(!mapList.isEmpty()) {
			for(SWUserBWMPMapping map : mapList) {
				SWApprovalHistoryReq history = new SWApprovalHistoryReq();
				User user = userService.findUserByUsername(String.valueOf(map.getUserId()));
				history.setDate(sdf.format(map.getCreatedDate()));
				history.setLevel(map.getLevel());
				history.setSupervisorId(String.valueOf(map.getUserId()));
				history.setSwId(String.valueOf(map.getSwId()));
				history.setLimit(user.getLimit());
				history.setName(user.getName());
				history.setRole(user.getRoleUser());
				historyList.add(history);
			}
		}
		return historyList;
	}
	
	/**
	 * Set Expired Date for link in email body <br />
	 * After specified date expired, links no longer accessible 
	 * @param dateParam instance of time ( d = day, m= month, y = year ) default d
	 * @param expiredParam value link should be expired based of dateParam
	 * @return expired date of links
	 * @author kusnendi
	 */
	private Date setExpiredDate(String dateParam,String expiredParam) {
		 int expired = Integer.parseInt(expiredParam);
		 Calendar cal = Calendar.getInstance();
		 if(dateParam.equalsIgnoreCase("d")) {
			 cal.add(Calendar.DATE, expired);
		 }else if(dateParam.equalsIgnoreCase("m")) {
			 cal.add(Calendar.MONTH, expired);
		 }else if(dateParam.equalsIgnoreCase("y")) {
			 cal.add(Calendar.YEAR, expired);
		 }else {
			 cal.add(Calendar.DATE, expired);
		 }
		return cal.getTime();
		
	}
	
	/**
	 * Get Parameter from T_MAIL_PARAM needed for mail engine
	 * @return HashMap contains mail parameter
	 * @author kusnendi
	 */
	private HashMap<String, String> getMailParam(){
		HashMap<String, String> mailParamHash = new HashMap<String,String>();
		List<MailParameter> mailParamList = mailParamSvc.getAll();
		for (MailParameter mailParam : mailParamList) {
			mailParamHash.put(mailParam.getMessageParam(), mailParam.getMessageValue());
		}
		return mailParamHash;
	}
	
	/**
	 * Check if links expired or not
	 * @param expiredDate date links should expired
	 * @return true if today date passed the expiration date
	 * @author kusnendi
	 */
	private boolean isDateExpired(Date expiredDate) {
		Calendar cal = Calendar.getInstance();
		if(expiredDate.getTime() - cal.getTime().getTime() <=0) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Calculate year between today and specified date
	 * @param date date that you want to compare the year
	 * @return year between today and specified date
	 * @author kusnendi
	 */
	private String compareYears(Date date) {
		LocalDateTime joinDateLocal = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime today = LocalDateTime.now(ZoneId.systemDefault());
		Long years = ChronoUnit.YEARS.between(joinDateLocal,today );
		return years.toString();
	}
	
	/**
	 * Format BigDecimal into Rupiah (IDR)
	 * @param number Number in BigDecimal
	 * @return formatted string
	 * @author kusnendi
	 */
	private String formatCurrency(BigDecimal number) {
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("Rp. ");
		symbols.setMonetaryDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		df.setDecimalFormatSymbols(symbols);
		double num = number.doubleValue();
		return df.format(num);
	}
	
	/**
	 * Format Double into Rupiah (IDR)
	 * @param number Number in Double
	 * @return formatted string
	 * @author kusnendi
	 */
	private String formatCurrency(Double number) {
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("Rp. ");
		symbols.setMonetaryDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		df.setDecimalFormatSymbols(symbols);
		return df.format(number);
	}
	
	/**
	 * Format BigDecimal into String with two digit decimal value
	 * @param number Number in BigDecimal
	 * @return Formatted String 
	 * @author Kusnendi
	 * 
	 */
	private String formatPercent(BigDecimal number) {
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("");
		symbols.setMonetaryDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		df.setDecimalFormatSymbols(symbols);
		double num = number.doubleValue();
		return df.format(num);
	}
	
	/**
	 * Format Double into String with two digit decimal value
	 * @param number Number in Double
	 * @return Formatted String 
	 * @author Kusnendi
	 * 
	 */
	private String formatPercent(Double number) {
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setCurrencySymbol("");
		symbols.setMonetaryDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		df.setDecimalFormatSymbols(symbols);
		return df.format(number);
	}
	
	/**
	 * Check if email address is valid or null
	 * @param email email address
	 * @return true if address is valid
     * @author kusnendi
	 */
	private boolean isValidEmail(String email) {
		final Pattern EMAIL_REGEX = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",Pattern.CASE_INSENSITIVE);
		if(email == null) {
			return false;
		}else {
			if(EMAIL_REGEX.matcher(email).matches()) {
				return true;
			}else {
				return false;
			}
		}
	}
    
    /**
     * Check if user role is CO or BM
     * @param role role code
     * @return true if user is BM
     * @author kusnendi
     */
	private boolean checkBmRole(String role) {
		if(role.equalsIgnoreCase("2")) {
			return true;
		}else {
			return false;
		}
	}
}